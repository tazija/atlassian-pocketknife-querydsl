package com.atlassian.pocketknife.internal.querydsl.testplugin.tables;

import com.atlassian.pocketknife.spi.querydsl.EnhancedRelationalPathBase;
import com.mysema.query.types.path.NumberPath;
import com.mysema.query.types.path.StringPath;

/**
 */
public class CustomerTable extends EnhancedRelationalPathBase<CustomerTable>
{
    // Columns
    public final NumberPath<Long> ID = createLongCol("ID").asPrimaryKey().build();
    public final StringPath FIRST_NAME = createStringCol("FIRST_NAME").notNull().build();
    public final StringPath LAST_NAME = createStringCol("LAST_NAME").notNull().build();
    public final NumberPath<Long> CREATED_TIME = createLongCol("CREATED_TIME").notNull().build();
    public final NumberPath<Long> MODIFIED_TIME = createLongCol("MODIFIED_TIME").notNull().build();


    public CustomerTable(final String tableName)
    {
        super(CustomerTable.class, tableName);
    }

}
