package com.atlassian.pocketknife.internal.querydsl.testplugin.tables;

/**
 * Our QueryDSL tables
 */
public class Tables
{
    private static final String AO = "AO_C0458A_";  // <-- namespace

    public static final CustomerTable CUSTOMER = new CustomerTable(AO + "CUSTOMER");
    public static final AddressTable ADDRESS = new AddressTable(AO + "ADDRESS");
}
