package com.atlassian.pocketknife.internal.querydsl.testplugin.tables;

import com.atlassian.pocketknife.spi.querydsl.EnhancedRelationalPathBase;
import com.mysema.query.types.path.NumberPath;
import com.mysema.query.types.path.StringPath;

/**
 */
public class AddressTable extends EnhancedRelationalPathBase<AddressTable>
{
    // Columns
    public final NumberPath<Long> ID = createLongCol("ID").asPrimaryKey().build();
    public final StringPath ADDRESS_TYPE = createStringCol("ADDRESS_TYPE").notNull().build();
    public final StringPath STREET_NAME1 = createStringCol("STREET_NAME1").notNull().build();
    public final StringPath STREET_NAME2 = createStringCol("STREET_NAME2").notNull().build();
    public final StringPath SUBURB = createStringCol("SUBURB").notNull().build();
    public final StringPath POST_CODE = createStringCol("POST_CODE").notNull().build();


    public AddressTable(final String tableName)
    {
        super(AddressTable.class, tableName);
    }

}
