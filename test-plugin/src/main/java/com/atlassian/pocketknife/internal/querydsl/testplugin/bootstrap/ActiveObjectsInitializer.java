package com.atlassian.pocketknife.internal.querydsl.testplugin.bootstrap;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import static org.springframework.util.Assert.notNull;

/**
 * Responsible for initializing ActiveObjects so that our database migrations are run before any QueryDSL queries.
 *
 * <p>
 * This is necessary for compatibility with versions of AO prior to 0.24, where AO is lazily initialized. Because we
 * don't run database queries directly via AO, it won't be initialized (and won't run migrations) unless we explicitly
 * run something on AO, which is what this class does.
 * </p>
 *
 */
@Component
public class ActiveObjectsInitializer
{
    private final ActiveObjects activeObjects;

    @Autowired
    public ActiveObjectsInitializer(ActiveObjects activeObjects)
    {
        notNull(activeObjects, "activeObjects");

        this.activeObjects = activeObjects;
    }

    /**
     * Called when the plugin is fully started
     */
    public void initialize()
    {
        activeObjects.flushAll();
    }

    public void uninitialize()
    {
    }
}
