package com.atlassian.pocketknife.internal.querydsl.testplugin.bootstrap;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;

/**
 */
@Scanned
public class OsgiImports
{
    @ComponentImport
    EventPublisher eventPublisher;

    @ComponentImport
    ActiveObjects activeObjects;
}
