package com.atlassian.pocketknife.internal.querydsl.testplugin.bootstrap;

import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.plugin.event.events.PluginEnabledEvent;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.pocketknife.api.util.runners.SealedRunner;
import com.atlassian.sal.api.lifecycle.LifecycleAware;
import com.google.common.annotations.VisibleForTesting;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import static com.google.common.collect.Lists.newArrayList;

/**
 * The Launcher is the starting point for the plugin.
 * <p/>
 * This should be the only life cycle listener in the system and it should then delegate out to other code as
 * appropriate.
 * <p/>
 * Its purpose is to allow us to reason about where events enter the system and the order of called code.  Having a
 * single listener / entry point object allows us to do that.
 */
@ExportAsService
@Component
public class Launcher implements LifecycleAware
{
    private static final Logger log = LoggerFactory.getLogger(Launcher.class);
    private static final String PKQDSL_TEST_PLUGIN = "Pocketknife QueryDSL test-plugin";

    private final static String PLUGIN_STARTED_EVENT = "plugin.started.event";
    private final static String LIFECYCLE_AWARE_ONSTART = "lifecycle.aware.onstart";
    private final static String PLUGIN_KEY = "com.atlassian.pocketknife.querydsl.testplugin";

    private final EventPublisher eventPublisher;
    private final ActiveObjectsInitializer aoInitializer;

    @Autowired
    public Launcher(
            final EventPublisher eventPublisher, final ActiveObjectsInitializer activeObjectsInitializer)
    {
        this.eventPublisher = eventPublisher;
        this.aoInitializer = activeObjectsInitializer;
        //
        // we want our logs to go out as info even if the host defaults to warn
        LogLeveller.setInfo(log);
    }

    private SealedRunner sealedStartCompleteRunner = new SealedRunner(newArrayList(PLUGIN_STARTED_EVENT, LIFECYCLE_AWARE_ONSTART), new Runnable()
    {
        @Override
        public void run()
        {
            onStartCompleted();
            ;
        }
    });

    @Override
    public void onStart()
    {
        sealedStartCompleteRunner.breakSeal(LIFECYCLE_AWARE_ONSTART);
    }

    @EventListener
    public final void onPluginEnabled(final PluginEnabledEvent pluginEnabledEvent)
    {
        if (pluginEnabledEvent.getPlugin().getKey().equals(PLUGIN_KEY))
        {
            sealedStartCompleteRunner.breakSeal(PLUGIN_STARTED_EVENT);
        }
    }

    @EventListener
    public final void onClearCache(final Object event)
    {
        //
        // Since we don't compile against JIRA we don't have a type safe class name to link against
        // hence the use of a direct string.   This is really important for testing and support
        //
        if (event.getClass().getName().equals("com.atlassian.jira.event.ClearCacheEvent"))
        {
            // until we have been fully started at least once, we don't respond to clear cache
            // this handles a case during initial setup of JIRA
            if (sealedStartCompleteRunner.hasRun())
            {
                onJiraClearCache();
            }
        }
    }

    @PostConstruct
    public void onSpringContextStarted()
    {
        log.info(PKQDSL_TEST_PLUGIN + " spring context is starting...");
        eventPublisher.register(this);
    }

    @VisibleForTesting
    void onStartCompleted()
    {
        log.info(PKQDSL_TEST_PLUGIN + " is initializing...");

        aoInitializer.initialize();

        log.info(PKQDSL_TEST_PLUGIN + " is initialized.");
    }

    /**
     * This is called during functional tests in JIRA to clear the state of the plugin for testing purposes.  Its a soft
     * reset of sorts.
     */
    private void onJiraClearCache()
    {
        log.info(PKQDSL_TEST_PLUGIN + " is clearing it caches...");

        // down
        aoInitializer.uninitialize();
        // up
        aoInitializer.initialize();

        log.info(PKQDSL_TEST_PLUGIN + " has cleared it caches.");
    }

    @PreDestroy
    public void onSpringContextStopped()
    {
        log.info(PKQDSL_TEST_PLUGIN + " spring context is stopping...");

        eventPublisher.unregister(this);
        aoInitializer.uninitialize();

        log.info(PKQDSL_TEST_PLUGIN + " spring context is stopped.");
    }
}
