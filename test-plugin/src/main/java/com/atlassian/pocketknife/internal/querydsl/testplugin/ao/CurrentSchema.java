package com.atlassian.pocketknife.internal.querydsl.testplugin.ao;

import net.java.ao.OneToMany;
import net.java.ao.schema.Indexed;
import net.java.ao.schema.NotNull;
import net.java.ao.schema.StringLength;
import net.java.ao.schema.Table;

import javax.annotation.Nullable;

/**
 * inActiveObjects paper work because we have to!
 * <p/>
 * Bow to the ORM of Slime, the ORM of Filth, the ORM of Putrescence.
 * <p/>
 * Boo. Boo. Rubbish. Filth. Slime. Muck. Boo. Boo. Boo.
 */
@SuppressWarnings ("unused")
public class CurrentSchema
{
    //================================================================================================================
    // CUSTOMER
    //================================================================================================================
    @Table ("customer")
    public interface CustomerAO extends EntityWithId
    {
        @NotNull
        @StringLength (DatabaseLimits.MAX_RUFLINS_450)
        String getFirstName();

        @NotNull
        @StringLength (DatabaseLimits.MAX_RUFLINS_450)
        String getLastName();

        @NotNull
        Long getCreatedTime();

        @NotNull
        Long getModifiedTime();

        @OneToMany (reverse = "getCustomer")
        AddressAO[] getAddresses();
    }


    //================================================================================================================
    // Address
    //================================================================================================================
    @Table ("address")
    public interface AddressAO extends EntityWithId
    {
        @NotNull
        CustomerAO getCustomer();

        @NotNull
        @StringLength (DatabaseLimits.TWO_RUFLINS_127)
        String getAddressType();

        @NotNull
        @StringLength (DatabaseLimits.TWO_RUFLINS_127)
        String getStreetName1();

        @StringLength (DatabaseLimits.TWO_RUFLINS_127)
        String getStreetName2();

        @NotNull
        @StringLength (DatabaseLimits.TWO_RUFLINS_127)
        String getSuburb();

        @NotNull
        @StringLength (DatabaseLimits.TWO_RUFLINS_127)
        String getPostCode();

    }
}
