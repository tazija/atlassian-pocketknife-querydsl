package com.atlassian.pocketknife.internal.querydsl.testplugin.rest;

import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.atlassian.pocketknife.api.querydsl.QueryFactory;
import com.atlassian.pocketknife.api.querydsl.SelectQuery;
import com.atlassian.pocketknife.api.querydsl.StreamyResult;
import com.atlassian.pocketknife.internal.querydsl.testplugin.tables.AddressTable;
import com.atlassian.pocketknife.internal.querydsl.testplugin.tables.Tables;
import com.google.common.base.Function;
import com.mysema.query.Tuple;

import java.util.Date;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import static com.atlassian.pocketknife.internal.querydsl.testplugin.tables.Tables.CUSTOMER;

@AnonymousAllowed
@Path("test")
public class TestResource
{
    private final QueryFactory queryFactory;

    public TestResource(final QueryFactory queryFactory)
    {
        this.queryFactory = queryFactory;
    }

    @AnonymousAllowed
    @GET
    public Response read(@Context UriInfo uriInfo)
    {
        readData();
        return Response.ok("read @" + new Date()).build();
    }

    @AnonymousAllowed
    @GET
    @Path ("badinit")
    public Response toEarly(@Context UriInfo uriInfo)
    {
        @SuppressWarnings ("UnusedDeclaration")
        AddressTable tooEarly = Tables.ADDRESS;
        return Response.ok("too early @" + new Date()).build();
    }

    private void readData()
    {

        Function<SelectQuery, StreamyResult> allCustomers = new Function<SelectQuery, StreamyResult>()
        {
            @Override
            public StreamyResult apply(final SelectQuery select)
            {
                return select.from(CUSTOMER).stream(CUSTOMER.all());
            }
        };
        try (StreamyResult result = queryFactory.select(allCustomers))
        {
            for (Tuple tuple : result)
            {
                System.out.println(tuple);
            }
        }

        final AddressTable afterWeHavePrimed = Tables.ADDRESS;
        Function<SelectQuery, StreamyResult> allAddresses = new Function<SelectQuery, StreamyResult>()
        {
            @Override
            public StreamyResult apply(final SelectQuery select)
            {
                return select.from(afterWeHavePrimed).stream(Tables.ADDRESS.all());
            }
        };
        try (StreamyResult result = queryFactory.select(allAddresses))
        {
            for (Tuple tuple : result)
            {
                System.out.println(tuple);
            }
        }
    }
}
