package com.atlassian.pocketknife.internal.querydsl;

import com.atlassian.pocketknife.api.querydsl.SchemaProvider;
import com.atlassian.pocketknife.internal.querydsl.schema.DatabaseSchemaCreation;
import com.atlassian.pocketknife.spi.querydsl.DialectConfiguration;
import org.junit.Before;
import org.junit.Test;

import java.sql.Connection;

import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class ConnectionPrimerImplTest
{
    DatabaseSchemaCreation databaseSchemaCreation;
    SchemaProvider schemaProvider;
    DialectConfiguration dialectConfiguration;
    private Connection connection;

    @Before
    public void setUp() throws Exception
    {
        databaseSchemaCreation = mock(DatabaseSchemaCreation.class);
        schemaProvider = mock(SchemaProvider.class);
        dialectConfiguration = mock(DialectConfiguration.class);

        connection = mock(Connection.class);
    }

    @Test
    public void test_important_side_effects() throws Exception
    {
        ConnectionPrimerImpl classUnderTest = new ConnectionPrimerImpl(databaseSchemaCreation, schemaProvider, dialectConfiguration);

        classUnderTest.prime(connection);

        verify(databaseSchemaCreation, atLeast(1)).prime();
        verify(schemaProvider, atLeast(1)).prime(connection);
        verify(dialectConfiguration, atLeast(1)).getDialectConfig(connection);

    }
}