package com.atlassian.pocketknife.internal.querydsl;

import com.atlassian.pocketknife.api.querydsl.ConnectionPrimer;

import java.sql.Connection;

public class MockConnectionPrimer implements ConnectionPrimer
{
    @Override
    public Connection prime(final Connection connection)
    {
        return connection;
    }
}
