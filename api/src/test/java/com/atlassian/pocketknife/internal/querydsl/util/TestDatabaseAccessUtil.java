package com.atlassian.pocketknife.internal.querydsl.util;

import com.atlassian.pocketknife.api.querydsl.ConnectionProvider;
import com.google.common.base.Function;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

@RunWith (MockitoJUnitRunner.class)
public class TestDatabaseAccessUtil
{

    @Mock
    private ConnectionProvider connectionProvider;

    @InjectMocks
    private DatabaseAccessUtil databaseAccessUtil;

    @Test
    public void test_doEnsurePrimed() throws Exception
    {
        ArgumentCaptor<Function> argument = ArgumentCaptor.forClass(Function.class);

        databaseAccessUtil.afterPropertiesSet();

        // primed
        DatabaseAccessUtil.ensurePrimed();

        verify(connectionProvider).withConnection(argument.capture());

        // it doesn't care about input, as long as applied
        argument.getValue().apply(null);

        // only once
        DatabaseAccessUtil.ensurePrimed();

        verifyNoMoreInteractions(connectionProvider);
    }

}
