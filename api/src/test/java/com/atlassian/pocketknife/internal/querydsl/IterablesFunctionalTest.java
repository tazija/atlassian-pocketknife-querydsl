package com.atlassian.pocketknife.internal.querydsl;

import com.atlassian.fugue.Effect;
import com.atlassian.fugue.Option;
import com.atlassian.pocketknife.api.querydsl.ClosePromise;
import com.atlassian.pocketknife.api.querydsl.CloseableIterable;
import com.atlassian.pocketknife.api.querydsl.CloseableIterables;
import com.atlassian.pocketknife.api.querydsl.QueryFactory;
import com.atlassian.pocketknife.api.querydsl.SchemaProvider;
import com.atlassian.pocketknife.api.querydsl.SelectQuery;
import com.atlassian.pocketknife.api.querydsl.StreamyResult;
import com.atlassian.pocketknife.internal.querydsl.tables.Connections;
import com.atlassian.pocketknife.spi.querydsl.DefaultDialectConfiguration;
import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.mysema.commons.lang.CloseableIterator;
import com.mysema.query.Tuple;
import org.junit.Before;
import org.junit.Test;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import static com.atlassian.pocketknife.internal.querydsl.tables.domain.QEmployee.employee;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;

/**
 * Tests the 'quasi-functional' aspects of CloseableIterables
 */
public class IterablesFunctionalTest
{
    public static final Function<Tuple, String> EMPLOYEE_FIRST_NAME_MAPPER = new Function<Tuple, String>()
    {
        @Override
        public String apply(final Tuple input)
        {
            return input.get(employee.firstname);
        }
    };
    public static final Predicate<String> CONTAINS_M = new Predicate<String>()
    {
        @Override
        public boolean apply(final String input)
        {
            return input.contains("M");
        }
    };

    private static final int TOTAL_NUMBER_EMPLOYEES_IN_DATASET = 10;

    private QueryFactory queryFactory;
    private CountingConnectionProvider countingConnectionProvider;

    @Before
    public void setUp() throws Exception
    {
        Connections.initHSQL();

        countingConnectionProvider = new CountingConnectionProvider();
        DefaultDialectConfiguration defaultDialectConfiguration = new DefaultDialectConfiguration(mock(SchemaProvider.class));
        queryFactory = new QueryFactoryImpl(countingConnectionProvider, defaultDialectConfiguration);

    }

    @Test
    public void testStreamyFetchFirst() throws Exception
    {
        StreamyResult streamyResult = streamEmployees();

        Option<Tuple> firstTuple = streamyResult.fetchFirst();
        assertThat(firstTuple.isDefined(), equalTo(true));

        assertThat(countingConnectionProvider.getBorrowCount(), equalTo(0));
    }

    @Test
    public void testStreamyMapExpression() throws Exception
    {
        StreamyResult streamyResult = streamEmployees();

        Option<String> firstName = streamyResult.map(employee.firstname).fetchFirst();
        assertThat(firstName.isDefined(), equalTo(true));
        assertThat(firstName.get(), equalTo("Mike"));
    }

    @Test
    public void testStreamyTake() throws Exception
    {
        StreamyResult streamyResult = streamEmployees();

        CloseableIterable<Tuple> taken = streamyResult.take(2);
        assertThat(consumerAndCount(taken), equalTo(2));
    }

    @Test
    public void testStreamyTakeWhile() throws Exception
    {
        StreamyResult streamyResult = streamEmployees();

        CloseableIterable<String> taken = streamyResult.map(EMPLOYEE_FIRST_NAME_MAPPER).takeWhile(CONTAINS_M);
        assertThat(consumerAndCount(taken), equalTo(2));
    }

    @Test
    public void testStreamyTakeWhenNotEnough() throws Exception
    {
        StreamyResult streamyResult = streamEmployees();

        CloseableIterable<Tuple> taken = streamyResult.take(2000);
        assertThat(consumerAndCount(taken), equalTo(10));

        // and it closed
        assertThat(countingConnectionProvider.getBorrowCount(), equalTo(0));
    }

    @Test
    public void testStreamyMapWithTake() throws Exception
    {
        StreamyResult streamyResult = streamEmployees();

        CloseableIterable<String> taken = streamyResult.map(EMPLOYEE_FIRST_NAME_MAPPER).take(2);
        assertThat(consumerAndCount(taken), equalTo(2));

        // and it closed
        assertThat(countingConnectionProvider.getBorrowCount(), equalTo(0));
    }

    @Test
    public void testStreamyMapWithTakeZero() throws Exception
    {
        StreamyResult streamyResult = streamEmployees();

        Function<Tuple, String> mapper = new Function<Tuple, String>()
        {
            @Override
            public String apply(final Tuple input)
            {
                return input.get(employee.firstname);
            }
        };
        CloseableIterable<String> taken = streamyResult.map(mapper).take(0);
        assertThat(consumerAndCount(taken), equalTo(0));

        // and it closed
        assertThat(countingConnectionProvider.getBorrowCount(), equalTo(0));
    }

    @Test
    public void testStreamyTakeWithMap() throws Exception
    {
        StreamyResult streamyResult = streamEmployees();

        Function<Tuple, String> mapper = new Function<Tuple, String>()
        {
            @Override
            public String apply(final Tuple input)
            {
                return input.get(employee.firstname);
            }
        };
        CloseableIterable<String> taken = streamyResult.take(2).map(mapper);
        assertThat(consumerAndCount(taken), equalTo(2));
    }

    @Test (expected = IllegalStateException.class)
    public void testStreamyTakeChecksForAlreadyTaken() throws Exception
    {
        StreamyResult streamyResult = streamEmployees();

        CloseableIterable<Tuple> taken = streamyResult.take(10);
        consume(taken, 3);
        taken.take(2);
    }

    @Test
    public void testStreamyMapWithMap() throws Exception
    {
        StreamyResult streamyResult = streamEmployees();

        Function<Tuple, String> mapper1 = new Function<Tuple, String>()
        {
            @Override
            public String apply(final Tuple input)
            {
                return input.get(employee.firstname);
            }
        };
        Function<String, Integer> mapper2 = new Function<String, Integer>()
        {
            @Override
            public Integer apply(final String input)
            {
                return input.length();
            }
        };
        Option<Integer> firstLength = streamyResult.map(mapper1).map(mapper2).fetchFirst();
        assertThat(firstLength.isDefined(), equalTo(true));
        assertThat(firstLength.get(), equalTo("Mike".length()));
    }

    @Test
    public void testStreamyFilter() throws Exception
    {
        StreamyResult streamyResult = streamEmployees();

        Predicate<String> notContainsM = Predicates.not(CONTAINS_M);

        CloseableIterable<String> taken = streamyResult.map(EMPLOYEE_FIRST_NAME_MAPPER).filter(notContainsM);
        assertThat(consumerAndCount(taken), equalTo(10 - 2));
    }

    @Test
    public void testStreamyFilterWithAFilter() throws Exception
    {
        StreamyResult streamyResult = streamEmployees();

        Predicate<String> containsJ = new Predicate<String>()
        {
            @Override
            public boolean apply(final String input)
            {
                return input.contains("J");
            }
        };
        Predicate<String> notContainsM = Predicates.not(CONTAINS_M);

        CloseableIterable<String> taken = streamyResult.map(EMPLOYEE_FIRST_NAME_MAPPER).filter(notContainsM).filter(containsJ);
        assertThat(consumerAndCount(taken), equalTo(3)); // Joe, Jim and Jennifer
    }


    @Test
    public void testStreamyForEachOnTuples() throws Exception
    {
        final List<Tuple> tuples = new ArrayList<>();

        try (StreamyResult streamyResult = streamEmployees())
        {
            streamyResult.foreach(new Effect<Tuple>()
            {
                @Override
                public void apply(final Tuple tuple)
                {
                    tuples.add(tuple);
                }
            });
        }
        assertThat(tuples.size(), equalTo(10));
    }

    @Test
    public void testStreamyForEachAfterMapping() throws Exception
    {
        final List<String> firstNames = new ArrayList<>();

        try (CloseableIterable<String> streamyResult = streamEmployees().map(employee.firstname))
        {
            streamyResult.foreach(new Effect<String>()
            {
                @Override
                public void apply(final String firstName)
                {
                    firstNames.add(firstName);
                }
            });
        }
        assertThat(firstNames.size(), equalTo(10));
    }


    @Test (expected = IllegalStateException.class)
    public void testStreamyFetchFirstThrowsExceptionWhenCalledTwice() throws Exception
    {
        StreamyResult streamyResult = streamEmployees();

        streamyResult.fetchFirst();
        streamyResult.fetchFirst();
    }

    @Test
    public void testIterablesPaysItsDebtsOnClose() throws Exception
    {
        final Connection connection = countingConnectionProvider.borrowConnection();
        ClosePromise closePromise = returnConnection(connection);

        CloseableIterator<String> iterator = iterateEmployeesFirstNames(connection);
        CloseableIterable<String> iterable = CloseableIterables.iterable(iterator, closePromise);

        int count = consumerAndCount(iterable);
        assertThat(count, equalTo(10));
        assertThat(0, equalTo(countingConnectionProvider.getBorrowCount()));
    }

    @Test
    public void testIterablesNoopDoesNotCloseAnything() throws Exception
    {
        final Connection connection = countingConnectionProvider.borrowConnection();

        CloseableIterator<String> iterator = iterateEmployeesFirstNames(connection);
        CloseableIterable<String> iterable = CloseableIterables.iterable(iterator);

        int count = consumerAndCount(iterable);
        assertThat(count, equalTo(10));
        assertThat(1, equalTo(countingConnectionProvider.getBorrowCount()));
    }

    @Test
    public void testCloseableIterablePaysItsDebtsOnClose() throws Exception
    {
        final Connection connection = countingConnectionProvider.borrowConnection();
        ClosePromise closePromise = returnConnection(connection);
        CloseableIterator<String> iterator = iterateEmployeesFirstNames(connection);
        CloseableIterable<String> iterable = CloseableIterables.iterable(iterator, closePromise);

        // close early without consuming
        iterable.getClosePromise().close();

        assertThat(0, equalTo(countingConnectionProvider.getBorrowCount()));
    }

    @Test
    public void testCloseableIterableHasNextDoesNotAffectResults() throws Exception
    {
        StreamyResult streamyResult = streamEmployees();

        final CloseableIterator<Tuple> iterator = streamyResult.iterator();
        iterator.hasNext();
        iterator.hasNext();

        int remainingCount = 0;
        while (iterator.hasNext())
        {
            iterator.next();
            remainingCount++;
        }

        assertThat(remainingCount, equalTo(TOTAL_NUMBER_EMPLOYEES_IN_DATASET));
    }

    @Test
    public void testCloseableIterableNextCanBeCalledWithoutHasNext() throws Exception
    {
        StreamyResult streamyResult = streamEmployees();

        final CloseableIterator<Tuple> iterator = streamyResult.iterator();
        for (int i = 0; i < TOTAL_NUMBER_EMPLOYEES_IN_DATASET; i++)
        {
            assertThat(iterator.next(), notNullValue());
        }
    }

    @Test (expected = NoSuchElementException.class)
    public void testCloseableIterableNextThrowsExceptionIfNoMoreElements() throws Exception
    {
        StreamyResult streamyResult = streamEmployees();

        final CloseableIterator<Tuple> iterator = streamyResult.iterator();
        for (int i = 0; i < TOTAL_NUMBER_EMPLOYEES_IN_DATASET + 1; i++)
        {
            assertThat(iterator.next(), notNullValue());
        }
    }

    private ClosePromise returnConnection(final Connection connection)
    {
        return new ClosePromise(new Runnable()
            {
                @Override
                public void run()
                {
                    countingConnectionProvider.returnConnection(connection);
                }
            });
    }


    private <T> int consumerAndCount(final CloseableIterable<T> taken)
    {
        int count = 0;
        for (T ignored : taken)
        {
            count++;
        }
        return count;
    }

    private <T> int consume(final CloseableIterable<T> taken, int n)
    {
        int count = 0;
        for (T ignored : taken)
        {
            if (count >= n)
            {
                break;
            }
            count++;
        }
        return count;
    }

    /**
     * There are 10 results in this result set
     *
     * @return employees order by id
     */
    private StreamyResult streamEmployees()
    {
        return queryFactory.select(new Function<SelectQuery, StreamyResult>()
        {
            @Override
            public StreamyResult apply(final SelectQuery select)
            {
                return select.from(employee).orderBy(employee.id.asc()).stream(employee.firstname, employee.lastname);
            }
        });
    }

    /**
     * There are 10 results in this result set
     *
     * @return employee first names order by id
     */
    private CloseableIterator<String> iterateEmployeesFirstNames(Connection connection)
    {
        return queryFactory.select(connection).from(employee).orderBy(employee.id.asc()).iterate(employee.firstname);
    }
}

