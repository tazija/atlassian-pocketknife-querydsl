package com.atlassian.pocketknife.internal.querydsl;

import com.atlassian.pocketknife.api.querydsl.SchemaProvider;
import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;

import java.sql.Connection;

/**
 * If your test calls on the SchemaProvider then this can mock it out
 */
public class SchemaProviderTestSupport implements TestRule
{

    @Override
    public Statement apply(final Statement base, final Description description)
    {
        initSchemaSupport();
        return base;
    }

    public static final SchemaProvider SIMPLE_SCHEMA_PROVIDER = new SchemaProvider()
    {
        @Override
        public String getSchema(final String logicalTableName)
        {
            return "schema";
        }

        @Override
        public String getTableName(final String logicalTableName)
        {
            return logicalTableName;
        }

        @Override
        public String getColumnName(final String logicalTableName, final String logicalColumnName)
        {
            return logicalColumnName;
        }

        @Override
        public void prime(final Connection connection)
        {
        }
    };

    /**
     * If we use EnhancedRelationalPath in our test code then we need to have it backed by a SchemaProvider.  In
     * production this is the case but in tests we need to make this call
     */
    public static void initSchemaSupport()
    {
        SchemaProviderAccessor.initializeWithSchemaProvider(SIMPLE_SCHEMA_PROVIDER);
    }

}
