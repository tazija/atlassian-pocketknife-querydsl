package com.atlassian.pocketknife.api.querydsl;

import com.atlassian.pocketknife.internal.querydsl.util.Unit;
import com.atlassian.pocketknife.spi.querydsl.AbstractConnectionProvider;
import com.google.common.base.Function;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InOrder;
import org.mockito.Mockito;

import java.sql.Connection;
import java.sql.SQLException;

import static org.mockito.Mockito.calls;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.mock;

public class ConnectionsTest
{

    private Connection connection;

    @Before
    public void setUp() throws Exception
    {
        connection = mock(Connection.class);
    }


    @Test
    public void testClose() throws Exception
    {
        Connections.close(connection);
        Mockito.verify(connection).close();

        Connections.close((Connection) null); // no problem
    }

    @Test
    public void testCloseSwallowsExceptions() throws Exception
    {
        doThrow(SQLException.class).when(connection).close();
        Connections.close(connection);
    }

    @Test
    public void test_abstract_provider_primes_connections() throws Exception
    {
        ConnectionPrimer connectionPrimer = mock(ConnectionPrimer.class);

        AbstractConnectionProvider connectionProvider = new AbstractConnectionProvider(connectionPrimer)
        {
            @Override
            protected Connection getConnectionImpl(final boolean autoCommit)
            {
                return connection;
            }
        };

        connectionProvider.borrowConnection();
        connectionProvider.borrowAutoCommitConnection();
        connectionProvider.withConnection(new Function<Connection, Unit>()
        {
            @Override
            public Unit apply(final Connection input)
            {
                return Unit.VALUE;
            }
        });

        InOrder inOrder = inOrder(connectionPrimer);
        inOrder.verify(connectionPrimer, calls(3)).prime(connection);

    }
}