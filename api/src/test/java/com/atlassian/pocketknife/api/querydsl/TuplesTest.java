package com.atlassian.pocketknife.api.querydsl;

import com.atlassian.pocketknife.internal.querydsl.SchemaProviderTestSupport;
import com.atlassian.pocketknife.spi.querydsl.EnhancedRelationalPathBase;
import com.google.common.base.Function;
import com.mysema.query.Tuple;
import com.mysema.query.types.Expression;
import com.mysema.query.types.path.BooleanPath;
import com.mysema.query.types.path.DatePath;
import com.mysema.query.types.path.DateTimePath;
import com.mysema.query.types.path.NumberPath;
import com.mysema.query.types.path.StringPath;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;
import javax.annotation.Nullable;

import static com.atlassian.pocketknife.api.querydsl.Tuples.column;
import static com.atlassian.pocketknife.api.querydsl.Tuples.toBigDecimal;
import static com.atlassian.pocketknife.api.querydsl.Tuples.toDouble;
import static com.atlassian.pocketknife.api.querydsl.Tuples.toFloat;
import static com.atlassian.pocketknife.api.querydsl.Tuples.toInt;
import static com.atlassian.pocketknife.api.querydsl.Tuples.toLong;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

public class TuplesTest
{
    private final TestTable TABLE = new TestTable();
    private TupleStream<String> stringStream;
    private TupleStream<Long> longStream;
    private TupleStream<Boolean> booleanStream;
    private TupleStream<Date> dateStream;
    private TupleStream<Timestamp> timestampStream;
    private Date now;

    @SuppressWarnings ("UnusedDeclaration")
    static class TestTable extends EnhancedRelationalPathBase<TestTable>
    {
        static {
            SchemaProviderTestSupport.initSchemaSupport();
        }

        NumberPath<Long> LONG_COL = createLongCol("LONG_COL").notNull().build();
        NumberPath<Integer> INTEGER_COL = createIntegerCol("INTEGER_COL").notNull().build();
        NumberPath<Float> FLOAT_COL = createFloatCol("FLOAT_COL").notNull().build();
        NumberPath<Double> DOUBLE_COL = createDoubleCol("DOUBLE_COL").notNull().build();
        NumberPath<BigDecimal> BIGDECIMAL_COL = createBigDecimal("BIGDECIMAL_COL");

        BooleanPath BOOLEAN_COL = createBoolean("BOOLEAN_COL");
        StringPath STRING_COL = createString("STRING_COL");


        DatePath<Date> DATE_COL = createDateCol("DATE_COL", Date.class).build();
        DateTimePath<Timestamp> DATETIME_COL = createDateTimeCol("DATETIME_COL", Timestamp.class).build();

        public TestTable()
        {
            super(TestTable.class, "testTable");
        }
    }

    @Before
    public void setUp() throws Exception
    {
        now = new Date();

        stringStream = new TupleStream<>("123");
        longStream = new TupleStream<>(123L);
        booleanStream = new TupleStream<>(true);
        dateStream = new TupleStream<>(now);
        timestampStream = new TupleStream<>(new Timestamp(now.getTime()));
    }

    @Test
    public void testMappingDirect() throws Exception
    {
        assertThat(stringStream.map(column(TABLE.STRING_COL)), equalTo("123"));
        assertThat(booleanStream.map(column(TABLE.BOOLEAN_COL)), equalTo(true));
        assertThat(dateStream.map(column(TABLE.DATE_COL)), equalTo(now));
        assertThat(timestampStream.map(column(TABLE.DATETIME_COL)), equalTo(new Timestamp(now.getTime())));

        assertThat(longStream.map(column(TABLE.LONG_COL)), equalTo(123L));
    }

    @Test
    public void testMappingWithConversion() throws Exception
    {
        assertThat(longStream.map(toLong(TABLE.LONG_COL)), equalTo(123L));
        assertThat(longStream.map(toInt(TABLE.LONG_COL)), equalTo(123));
        assertThat(longStream.map(toDouble(TABLE.LONG_COL)), equalTo(123d));
        assertThat(longStream.map(toFloat(TABLE.LONG_COL)), equalTo(123f));
        assertThat(longStream.map(toBigDecimal(TABLE.LONG_COL)), equalTo(new BigDecimal(123)));

        assertThat(longStream.map(toLong(TABLE.INTEGER_COL)), equalTo(123L));
        assertThat(longStream.map(toInt(TABLE.INTEGER_COL)), equalTo(123));
        assertThat(longStream.map(toDouble(TABLE.INTEGER_COL)), equalTo(123d));
        assertThat(longStream.map(toFloat(TABLE.INTEGER_COL)), equalTo(123f));
        assertThat(longStream.map(toBigDecimal(TABLE.INTEGER_COL)), equalTo(new BigDecimal(123)));

    }

    class TupleStream<T>
    {
        private final Tuple tuple;

        TupleStream(final T value)
        {
            this.tuple = new TestTuple(value);
        }


        private <C> C map(Function<Tuple, C> f)
        {
            return f.apply(tuple);
        }
    }

    @SuppressWarnings ("unchecked")
    class TestTuple implements Tuple
    {
        private final Object[] objects;

        TestTuple(final Object singleObj)
        {
            this.objects = new Object[] { singleObj };
        }

        @Nullable
        @Override
        public <T> T get(final Expression<T> expr)
        {
            return (T) objects[0];
        }

        @Nullable
        @Override
        public <T> T get(final int index, final Class<T> type)
        {
            return (T) objects[0];
        }

        @Override
        public int size()
        {
            return 1;
        }

        @Override
        public Object[] toArray()
        {
            return objects;
        }
    }


}