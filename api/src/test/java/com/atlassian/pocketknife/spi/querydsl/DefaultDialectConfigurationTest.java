package com.atlassian.pocketknife.spi.querydsl;

import com.atlassian.fugue.Pair;
import com.atlassian.pocketknife.api.querydsl.DialectProvider;
import com.atlassian.pocketknife.api.querydsl.SchemaProvider;
import com.atlassian.pocketknife.internal.querydsl.DialectHelper;
import com.mysema.query.sql.HSQLDBTemplates;
import com.mysema.query.sql.MySQLTemplates;
import com.mysema.query.sql.OracleTemplates;
import com.mysema.query.sql.PostgresTemplates;
import com.mysema.query.sql.SQLServer2005Templates;
import com.mysema.query.sql.SQLServerTemplates;
import com.mysema.query.sql.SQLTemplates;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.Map;

import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class DefaultDialectConfigurationTest
{

    private DefaultDialectConfiguration dialectConfiguration;
    private DatabaseMetaData databaseMetaData;
    private Connection connection;

    @Before
    public void setUp() throws Exception
    {
        connection = mock(Connection.class);
        databaseMetaData = mock(DatabaseMetaData.class);

        when(connection.getMetaData()).thenReturn(databaseMetaData);
    }

    /**
     * As we build out our database support we should expand this class
     */
    @Test
    public void testDatabaseDetectionCoverage() throws Exception
    {
        Map<String, Class> support = new LinkedHashMap<>();
        support.put(":postgresql:", PostgresTemplates.Builder.class);
        support.put(":oracle:", OracleTemplates.Builder.class);
        support.put(":hsqldb:", HSQLDBTemplates.Builder.class);
        support.put(":sqlserver:", SQLServerTemplates.Builder.class);
        support.put(":mysql:", MySQLTemplates.Builder.class);

        for (String connStr : support.keySet())
        {
            assertDatabaseType(support.get(connStr), connStr);
        }
    }

    @Test (expected = UnsupportedOperationException.class)
    public void testDatabaseDetectionUnknownDB() throws Exception
    {
        assertDatabaseType(OracleTemplates.Builder.class, "SomeDatabaseWeDon'tSupport");
    }

    @Test
    public void testDatabaseDetectionRecoversFromTransientErrors() throws Exception
    {
        final SQLException sqle = new SQLException("Just testing!");
        when(connection.getMetaData())
                .thenThrow(sqle)
                .thenReturn(databaseMetaData);

        try
        {
            assertDatabaseType(PostgresTemplates.Builder.class, ":postgresql:");
            fail("Expected a LazyReference.InitializationException");
        }
        catch (RuntimeException ie)
        {
            assertEquals(sqle, ie.getCause());
        }

        assertThat(dialectConfiguration.getDialectConfig(connection), Matchers.notNullValue());
    }

    @Test
    public void testDatabaseDetectionForSideEffect() throws Exception
    {
        assertDatabaseType(PostgresTemplates.Builder.class, ":postgresql:");
        assertThat(dialectConfiguration.getDialectConfig(connection), Matchers.notNullValue());
    }

    @Test
    public void test_get_db_template_for_sqlserver_2005() throws SQLException
    {
        String sqlServerConnStr = "jdbc:jtds:sqlserver://dbserver:1433/schema";
        Class<SQLServer2005Templates> expectedSQLServerTemplate = SQLServer2005Templates.class;

        when(databaseMetaData.getDatabaseMajorVersion()).thenReturn(DialectHelper.SQLSERVER_2005);

        assertDatabaseType(SQLServer2005Templates.builder().getClass(), sqlServerConnStr);

        Pair<SQLTemplates.Builder, DialectProvider.SupportedDatabase> dbTemplate = dialectConfiguration.getDBTemplate(sqlServerConnStr, databaseMetaData);
        assertThat(dbTemplate, notNullValue());
        assertThat(dbTemplate.left().build(), Matchers.instanceOf(expectedSQLServerTemplate));
        assertThat(dbTemplate.right(), Matchers.instanceOf(DialectProvider.SupportedDatabase.SQLSERVER.getClass()));
    }

    @Test
    public void test_get_db_template_for_non_sqlserver() throws SQLException
    {
        String serverConnStr = "jdbc:mysql://dbserver:3306/schema";
        Class<MySQLTemplates> expectedServerTemplate = MySQLTemplates.class;

        assertDatabaseType(MySQLTemplates.builder().getClass(), serverConnStr);

        Pair<SQLTemplates.Builder, DialectProvider.SupportedDatabase> dbTemplate = dialectConfiguration.getDBTemplate(serverConnStr, databaseMetaData);
        assertThat(dbTemplate, Matchers.notNullValue());
        assertThat(dbTemplate.left().build(), Matchers.instanceOf(expectedServerTemplate));
        assertThat(dbTemplate.right(), Matchers.instanceOf(DialectProvider.SupportedDatabase.MYSQL.getClass()));
    }

    @Test
    public void test_side_effects_called() throws Exception
    {
        // assemble
        SchemaProvider schemaProvider = mock(SchemaProvider.class);
        when(databaseMetaData.getURL()).thenReturn("jdbc:mysql://dbserver:3306/schema");
        dialectConfiguration = new DefaultDialectConfiguration(schemaProvider);

        // act
        dialectConfiguration.getDialectConfig(connection);

        // assert
        // I know this is asserting behaviour INSIDE the class but these are important side effects right now
        Mockito.verify(schemaProvider, atLeast(1)).prime(connection);
    }

    private void assertDatabaseType(final Class builderClass, String connStr)
            throws SQLException
    {
        dialectConfiguration = new DefaultDialectConfiguration(mock(SchemaProvider.class))
        {
            @Override
            public SQLTemplates.Builder enrich(final SQLTemplates.Builder builder)
            {
                assertThat(builder, Matchers.instanceOf(builderClass));
                return super.enrich(builder);
            }
        };
        when(databaseMetaData.getURL()).thenReturn(connStr);
        DialectProvider.Config config = dialectConfiguration.getDialectConfig(connection);

        assertThat(config.getSqlTemplates().isUseQuotes(), equalTo(true));
    }
}