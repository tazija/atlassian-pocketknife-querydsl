package com.atlassian.pocketknife.api.querydsl;

import com.atlassian.pocketknife.internal.querydsl.CloseableIterableImpl;
import com.google.common.base.Functions;
import com.mysema.commons.lang.CloseableIterator;

/**
 * A helper class for {@link CloseableIterables} and their related classes, with the ability to take
 * QueryDSL {@link com.mysema.commons.lang.CloseableIterator}s and turn them into {@link com.atlassian.pocketknife.api.querydsl.CloseableIterable}s
 * which have extra semantics, close promises and functional helper methods.
 *
 */
public class CloseableIterables
{
    /**
     * Returns a {@link CloseableIterable} from the specified queryDSL {@link CloseableIterator}
     * <p/>
     * NOTE : Now that this is a once only Iterable. Its backed by a database connection and hence
     * it can only be consumed once.
     *
     * @param closeableIterator the source iterator that is closeable
     * @param closePromise - the promise that should be executed when the iterator is finished
     * @param <T> the type of object in the iterator
     * @return a CloseableIterable
     */
    public static <T> CloseableIterable<T> iterable(CloseableIterator<T> closeableIterator, ClosePromise closePromise)
    {
        return new CloseableIterableImpl<>(closeableIterator, Functions.<T>identity(), closePromise);
    }

    /**
     * Returns a {@link CloseableIterable} from the specified queryDSL {@link CloseableIterator}
     * <p/>
     * NOTE : Now that this is a once only Iterable.
     *
     * @param closeableIterator the source iterator that is closeable
     * @param <T> the type of object in the iterator
     * @return a CloseableIterable
     */
    public static <T> CloseableIterable<T> iterable(CloseableIterator<T> closeableIterator)
    {
        return new CloseableIterableImpl<>(closeableIterator, Functions.<T>identity(), ClosePromise.NOOP());
    }
}
