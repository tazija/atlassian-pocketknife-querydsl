package com.atlassian.pocketknife.api.querydsl;

import com.atlassian.annotations.Internal;
import com.atlassian.annotations.PublicApi;

import java.sql.Connection;

/**
 * A helper that can be used to provide database names (schema, table, column) at runtime.
 *
 * Useful for retrieving names in environments where case changes between database engines,
 * or where names aren't known ahead of time.
 */
@PublicApi
public interface SchemaProvider
{
    /**
     * Support to retrieve schema name of the given table
     *
     * @param logicalTableName the logical name of the table
     *
     * @return the schema name associated to the table, or empty if none is available
     */
    String getSchema(String logicalTableName);

    /**
     * Retrieve the case-sensitive table name of the actual table that matches the given table name (if it exists)
     *
     * @param logicalTableName the logical table to lookup
     *
     * @return The actual (case-sensitive) table name for the given table, or <code>null</code> if none exists.
     */
    String getTableName(String logicalTableName);

    /**
     * Retrieve the case-sensitive column name of the column in the given table (if it exists)
     *
     * @param logicalTableName the logical table to look for columns in
     * @param logicalColumnName the logical column to lookup
     *
     * @return The actual (case-sensitive) column name for the column in the given table,
     * or <code>null</code> if none is found.
     */
    String getColumnName(String logicalTableName, String logicalColumnName);

    /**
     * This can be called to prime the internal state of the schema provider without it having to obtain a connection.
     *
     * @param connection a connection to use
     */
    @Internal
    void prime(Connection connection);
}
