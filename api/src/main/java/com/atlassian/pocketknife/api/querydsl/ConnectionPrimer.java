package com.atlassian.pocketknife.api.querydsl;

import java.sql.Connection;

/**
 * ConnectionPrimer is responsible for priming the connection and hence caches of database dialect and configuration.
 * <p>
 * There have been a lot of deadlocks in production because of locked class initialization and connection pooling
 * starvation and so this MUST be called to prime the schema information.  {@link com.atlassian.pocketknife.api.querydsl.ConnectionProvider}
 * should call on this before handing out a connection.
 *
 * @since v0.x
 */
public interface ConnectionPrimer
{
    /**
     * This is called to prime the caches of database dialect and configuration.
     *
     * @param connection the connection to use to prime the database info
     * @return a primed connection
     */
    Connection prime(Connection connection);
}
