package com.atlassian.pocketknife.api.querydsl;

import java.io.Closeable;
import javax.annotation.concurrent.NotThreadSafe;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * A ClosePromise will call the {@link #closeEffect()} once and only once. Once closed it is considered
 * closed forever.
 * <p/>
 * This promise is not thread safe.
 */
@NotThreadSafe
public class ClosePromise implements Closeable
{
    private static final Runnable NOOP_RUNNABLE = new Runnable()
    {
        @Override
        public void run()
        {
        }
    };
    /**
     * A close promise that does nothing. Since Close Promises are mutable you need a new one every time
     */
    public static ClosePromise NOOP()
    {
        return new ClosePromise();
    }

    private boolean closed = false;
    private final ClosePromise parentPromise;
    private final Runnable closeEffect;

    public ClosePromise()
    {
        this(null, NOOP_RUNNABLE);
    }

    /**
     * A promise to do something when this promise is closed
     *
     * @param closeEffect what happens when the promise is closed
     */
    public ClosePromise(Runnable closeEffect)
    {
        this(NOOP(), closeEffect);
    }

    /**
     * You can chain together promises such that the child is closed first then the parent
     *
     * @param parentPromise the parent to close after yourself
     * @param closeEffect what happens when the promise is closed
     */
    public ClosePromise(ClosePromise parentPromise, Runnable closeEffect)
    {
        this.parentPromise = parentPromise;
        this.closeEffect = checkNotNull(closeEffect);
    }


    @Override
    public void close()
    {
        if (!closed)
        {
            closed = true;
            closeEffect();
        }
        if (parentPromise != null)
        {
            parentPromise.close();
        }
    }

    public boolean isClosed()
    {
        return closed;
    }

    private void closeEffect()
    {
        closeEffect.run();
    }
}
