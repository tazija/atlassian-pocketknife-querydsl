package com.atlassian.pocketknife.api.querydsl;

import com.atlassian.annotations.Internal;
import com.google.common.base.Function;
import com.mysema.query.Tuple;
import com.mysema.query.types.Expression;
import com.mysema.query.types.expr.NumberExpression;

import java.math.BigDecimal;

/**
 * Functions to map values from within a Tuple
 */
@Internal
class TupleMapper
{
    <T> Function<Tuple, T> column(final Expression<T> expr)
    {
        return new Function<Tuple, T>()
        {
            @Override
            public T apply(final Tuple tuple)
            {
                T t = tuple.get(expr);
                return t == null ? null : t;
            }
        };
    }

    <T extends Number & Comparable<?>> Function<Tuple, Integer> toInt(final NumberExpression<T> expr)
    {
        return new Function<Tuple, Integer>()
        {
            @Override
            public Integer apply(final Tuple tuple)
            {
                T t = tuple.get(expr);
                return t == null ? null : t.intValue();
            }
        };
    }

    <T extends Number & Comparable<?>> Function<Tuple, Long> toLong(final NumberExpression<T> expr)
    {
        return new Function<Tuple, Long>()
        {
            @Override
            public Long apply(final Tuple tuple)
            {
                T t = tuple.get(expr);
                return t == null ? null : t.longValue();
            }
        };
    }

    <T extends Number & Comparable<?>> Function<Tuple, Float> toFloat(final NumberExpression<T> expr)
    {
        return new Function<Tuple, Float>()
        {
            @Override
            public Float apply(final Tuple tuple)
            {
                T t = tuple.get(expr);
                return t == null ? null : t.floatValue();
            }
        };
    }

    <T extends Number & Comparable<?>> Function<Tuple, Double> toDouble(final NumberExpression<T> expr)
    {
        return new Function<Tuple, Double>()
        {
            @Override
            public Double apply(final Tuple tuple)
            {
                T t = tuple.get(expr);
                return t == null ? null : t.doubleValue();
            }
        };
    }

    <T extends Number & Comparable<?>> Function<Tuple, BigDecimal> toBigDecimal(final NumberExpression<T> expr)
    {
        return new Function<Tuple, BigDecimal>()
        {
            @Override
            public BigDecimal apply(final Tuple tuple)
            {
                T t = tuple.get(expr);
                if (t instanceof BigDecimal)
                {
                    return (BigDecimal) t;
                }
                return t == null ? null : new BigDecimal(t.doubleValue());
            }
        };
    }
}
