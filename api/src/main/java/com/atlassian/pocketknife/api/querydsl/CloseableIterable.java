package com.atlassian.pocketknife.api.querydsl;

import com.atlassian.fugue.Effect;
import com.atlassian.fugue.Option;
import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.mysema.commons.lang.CloseableIterator;

import java.io.Closeable;

/**
 * A closeable iterable can be closed.  This is uber important in the SQL world because connections and results MUST be
 * closed when the results have been consumed.
 * <p/>
 * They must also be closed in try finally blocks as well for defensive reasons such as your code throwing exceptions
 * during its processing.
 */
public interface CloseableIterable<T> extends Iterable<T>, Closeable
{
    /**
     * @return the closeable iterator used under the covers
     */
    CloseableIterator<T> iterator();


    /**
     * Returns the underlying close promise that will be run when the Iterable is consumed.  Normally on JDK 7 you will not need this
     * since CloseableIterable is itself Closeable and you can put it in try() statements. But if you must call the underlying close promise
     * you can.
     *
     * @return the underlying close promise that will be run when the Iterable is consumed
     */
    ClosePromise getClosePromise();

    /**
     * This will fetch the first result in the iterable and then call {@link #close()}, ensuring that after this
     * method is called, the underlying iterator is always closed.
     *
     * @return the first result or an option of none if there is no results
     */
    Option<T> fetchFirst();

    /**
     * Returns the n first elements of this Iterable as another Iterable, or else the whole Iterable, if it has less
     * than n elements.
     * <p/>
     * NOTE - once the iterable reaches the limit n it will be automatically closed
     *
     * NOTE - You really should push most limiting into the SQL query layer via a limit(n) statement on the {@link com.atlassian.pocketknife.api.querydsl.SelectQuery}.
     * Relational databases  will always do a better job of this if its possible.  This method
     * should only be used when its NOT possible to limit in SQL
     *
     * @param n the number of elements to return - this must be >= to 0
     * @return a limited Iterable
     * @throws java.lang.IllegalStateException if n is < 0
     */
    CloseableIterable<T> take(int n);

    /**
     * Returns the elements until such time as the predicate returns false or the underlying elements run out
     * <p/>
     * NOTE - once the Predicate returns false the Iterable it will be automatically closed
     *
     * NOTE - You really should push most limiting into the SQL query layer via a limit(n) statement on the {@link com.atlassian.pocketknife.api.querydsl.SelectQuery}.
     * Relational databases  will always do a better job of this if its possible.  This method
     * should only be used when its NOT possible to limit in SQL
     *
     * @param takeWhilePredicate the predicate to evaluate against each element
     * @return a limited Iterable
     */
    CloseableIterable<T> takeWhile(Predicate<T> takeWhilePredicate);

    /**
     * This will filter out objects that don't match the specified predicate.
     * <p/>
     * NOTE : You really should push most filtering into the SQL query layer.  Relational databases will always do a
     * better job of this if its possible.  This filter method should only be used when its NOT possible to filter in
     * SQL
     *
     * @param filterPredicate the predicate to apply
     * @return an iterable of domain objects that match the predicate
     */
    CloseableIterable<T> filter(Predicate<T> filterPredicate);

    /**
     * This will map a fetch query of <T> objects into an Iterable of domain object D
     *
     * @param mapper the tuple mapper function
     * @param <D> the domain type
     * @return an iterable of domain objects
     */
    <D> CloseableIterable<D> map(Function<T, D> mapper);

    /**
     * Calls the effect for each result in the CloseableIterable
     *
     * @param effect the side effect code to be called
     */
    void foreach(final Effect<T> effect);

    /**
     * Closes this iterator and releases any system resources associated with it. If the iterator is already closed then
     * invoking this method has no effect.
     */
    void close();

}
