package com.atlassian.pocketknife.api.querydsl;

import com.atlassian.annotations.PublicApi;
import com.atlassian.pocketknife.internal.querydsl.util.DatabaseAccessUtil;

/**
 * A component to ensure database access is primed and ready to go.  This class is needed as a "guard" because we publish
 * OSGi services before we are in a position to honour that.
 *
 * So we must guard each database call with this to ensure we have initialised
 *
 * @since v0.82
 */
@PublicApi
public class DatabaseAccess
{
    /**
     * Ensure the database and the library code that uses it is primed and ready to go
     */
    public static void ensurePrimed() {
        DatabaseAccessUtil.ensurePrimed();
    }
}
