package com.atlassian.pocketknife.api.querydsl;

import com.atlassian.fugue.Function2;
import com.google.common.base.Function;
import com.mysema.query.Tuple;
import com.mysema.query.types.Expression;

/**
 * A StreamedResult is one that does NOT load all the SQL data into memory but rather lazily maps from database result
 * objects into domain objects.
 * <p/>
 * Its uber important that you close this object (either via the mapped iterable or directly on this object) to ensure
 * that connections and result sets are properly closed.
 *
 * By default the streamy result is a closeable iterator of Tuples.  You can turn it into other things by using the
 * map and other methods.
 */
public interface StreamyResult extends CloseableIterable<Tuple>
{
    /**
     * This will map a fetch query of Tuple objects into an Iterable of domain object T
     *
     * @param mapper the tuple mapper function
     * @param <T> the domain type
     * @return an iterable of domain objects
     */
    <T> CloseableIterable<T> map(Function<Tuple, T> mapper);

    /**
     * This will map a fetch query of Tuple objects into an Iterable of domain object T using the expression contained
     * within each Tuple
     *
     * @param expr the SimpleExpression contained within the Tuple
     * @param <T> the domain type
     * @return an iterable of domain objects
     */
    <T> CloseableIterable<T> map(Expression<T> expr);

    /**
     * Fold the combiningFunction over the query and return the accumulated value, executes immediately and closes the
     * iterable.
     *
     * @param initial The initial value to be passed to #combiningFunction
     * @param combiningFunction A function that takes a tuple and the accumulating value and returns the new accumulating value
     * @return The result of applying combiningFunction to each element in the list starting with #initial and passing in the result of
     * the previous call as it traverses the result.
     */
    <T> T foldLeft(T initial, Function2<T, Tuple, T> combiningFunction);

    /**
     * Closes this result and releases any system resources associated with it. If the object is already closed then
     * invoking this method has no effect.
     */
    void close();

}
