package com.atlassian.pocketknife.spi.querydsl;

import com.atlassian.annotations.PublicSpi;
import com.atlassian.fugue.Pair;
import com.atlassian.pocketknife.api.querydsl.LoggingSqlListener;
import com.atlassian.pocketknife.api.querydsl.SchemaProvider;
import com.atlassian.pocketknife.internal.querydsl.DialectHelper;
import com.atlassian.pocketknife.internal.querydsl.util.MemoizingResettingReference;
import com.google.common.base.Function;
import com.mysema.query.sql.Configuration;
import com.mysema.query.sql.H2Templates;
import com.mysema.query.sql.HSQLDBTemplates;
import com.mysema.query.sql.NuoDBTemplates;
import com.mysema.query.sql.MySQLTemplates;
import com.mysema.query.sql.OracleTemplates;
import com.mysema.query.sql.PostgresTemplates;
import com.mysema.query.sql.SQLTemplates;
import com.mysema.query.sql.types.AbstractType;
import com.mysema.query.sql.types.Null;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.annotation.Nullable;

import static org.apache.commons.lang.ArrayUtils.isNotEmpty;

/**
 * This is a dialect configuration that you can use to detect and build the QueryDSL dialect config.
 * <p/>
 * You can use this class to override the way the configuration is built via the {@link
 * #enrich(com.mysema.query.sql.SQLTemplates.Builder)} and {@link #enrich(com.mysema.query.sql.Configuration)} methods
 */
@Component
@PublicSpi
public class DefaultDialectConfiguration implements DialectConfiguration
{
    private static final Logger log = LoggerFactory.getLogger(DefaultDialectConfiguration.class);

    private final SchemaProvider schemaProvider;
    private final MemoizingResettingReference<Connection,Config> configurationReference = new MemoizingResettingReference<>(new Function<Connection, Config>()
    {
        @Override
        public Config apply(@Nullable final Connection connection)
        {
            return detect(connection);
        }
    });

    @Autowired
    public DefaultDialectConfiguration(final SchemaProvider schemaProvider)
    {
        this.schemaProvider = schemaProvider;
    }

    @Override
    public Config getDialectConfig(Connection connection)
    {
        //
        // technically we don't need this information here however we know that once we have a connection we want to prime
        // the internal cache so we don't have the n+1 connection deadlocks we have seen in production.  So we tie
        // these two classes together at this point.  It breaks encapsulation somewhat but it means it avoids deadlocks
        // and that's the lesser of two evils.
        //
        // I have chosen DialectConfiguration as the single entry point rather than spread around the required calls to each call site.
        //
        schemaProvider.prime(connection);

        // now get the dialect config for the database
        return configurationReference.get(connection);
    }

    private Config detect(final Connection connection)
    {
        Pair<SQLTemplates, SupportedDatabase> pair = buildTemplates(connection);
        SQLTemplates sqlTemplates = pair.left();
        Configuration configuration = enrich(new Configuration(sqlTemplates));
        configuration.addListener(new LoggingSqlListener(configuration));

        // Should be removed when https://github.com/querydsl/querydsl/issues/1079 is fixed
        configuration.register(new NullType());
        return new Config(sqlTemplates, configuration, buildDatabaseInfo(pair.right(), connection));
    }

    @Override
    public SQLTemplates.Builder enrich(final SQLTemplates.Builder builder)
    {
        return builder
                .newLineToSingleSpace()
                .quote();

    }

    @Override
    public Configuration enrich(final Configuration configuration)
    {
        return configuration;
    }

    private static Map<String, Pair<SQLTemplates.Builder, SupportedDatabase>> support = new LinkedHashMap<>();
    static
    {
        support.put(DialectHelper.DB_URL_IDENTIFIER_POSTGRES, Pair.pair(PostgresTemplates.builder(), SupportedDatabase.POSTGRESSQL));
        support.put(DialectHelper.DB_URL_IDENTIFIER_ORACLE, Pair.pair(OracleTemplates.builder(), SupportedDatabase.ORACLE));
        support.put(DialectHelper.DB_URL_IDENTIFIER_HSQLDB, Pair.pair(HSQLDBTemplates.builder(), SupportedDatabase.HSQLDB));
        support.put(DialectHelper.DB_URL_IDENTIFIER_MYSQL, Pair.pair(MySQLTemplates.builder(), SupportedDatabase.MYSQL));
        support.put(DialectHelper.DB_URL_IDENTIFIER_H2, Pair.pair(H2Templates.builder(), SupportedDatabase.H2));
        support.put(DialectHelper.DB_URL_IDENTIFIER_NUODB, Pair.pair(NuoDBTemplates.builder(), SupportedDatabase.NUODB));
    }

    private Pair<SQLTemplates, SupportedDatabase> buildTemplates(final Connection connection)
    {
        try
        {
            DatabaseMetaData metaData = connection.getMetaData();
            String connStr = metaData.getURL();
            Pair<SQLTemplates.Builder, SupportedDatabase> pair = getDBTemplate(connStr, metaData);
            if (pair == null)
            {
                throw new UnsupportedOperationException(String.format("Unable to detect QueryDSL template support for database %s", connStr));
            }
            SQLTemplates templates = enrich(pair.left()).build();
            return Pair.pair(templates, pair.right());

        }
        catch (SQLException e)
        {
            throw new RuntimeException("Unable to enquire on JDBC metadata to configure QueryDSL", e);
        }
    }

    /**
     * package private for testing
     */
    @Nullable
    Pair<SQLTemplates.Builder, SupportedDatabase> getDBTemplate(final String connStr, final DatabaseMetaData metaData) throws SQLException
    {
        Pair<SQLTemplates.Builder, SupportedDatabase> sqlTemplatePair;
        // instantiate SQLServerTemplate base on its version
        if (DialectHelper.isSQLServer(connStr))
        {
            sqlTemplatePair = DialectHelper.getSQLServerDBTemplate(metaData);
        }
        else
        {
            sqlTemplatePair = getStaticSupportedDBTemplate(connStr);
        }

        if (sqlTemplatePair != null)
        {
            log.debug("SQL template has been initialized successfully {}", sqlTemplatePair.toString());
        }
        else
        {
            log.warn("System was unable to initialize SQL template for {}", connStr);
        }

        return sqlTemplatePair;
    }

    /**
     * Get supported DB template base on connection string except SQLServer
     *
     * <p>
     * package private for testing
     */
    @Nullable
    Pair<SQLTemplates.Builder, SupportedDatabase> getStaticSupportedDBTemplate(final String connStr)
    {
        Pair<SQLTemplates.Builder, SupportedDatabase> pair = null;

        // which databases do we support
        for (String db : support.keySet())
        {
            if (connStr.contains(db))
            {
                pair = support.get(db);
                break;
            }
        }
        return pair;
    }

    private DatabaseInfo buildDatabaseInfo(final SupportedDatabase supportedDatabase, final Connection connection)
    {
        try
        {
            DatabaseMetaData metaData = connection.getMetaData();

            return new DatabaseInfo(supportedDatabase,
                    metaData.getDatabaseProductName(),
                    metaData.getDatabaseProductVersion(),
                    metaData.getDatabaseMajorVersion(),
                    metaData.getDatabaseMinorVersion(),
                    metaData.getDriverName(),
                    metaData.getDriverMajorVersion(),
                    metaData.getDriverMinorVersion());
        }
        catch (SQLException e)
        {
            throw new RuntimeException("Unable to enquire on JDBC metadata to determine DatabaseInfo", e);
        }
    }

    // Workaround allowing us to log SQL with null in it (e.g. update table_x set col_1 to null)
    // Should be removed when https://github.com/querydsl/querydsl/issues/1079 is fixed
    private static class NullType extends AbstractType<Null>
    {
        public NullType() {
            super(Types.NULL);
        }

        @Override
        public Null getValue(ResultSet rs, int startIndex) throws SQLException {
            return Null.DEFAULT;
        }

        @Override
        public Class<Null> getReturnedClass() {
            return Null.class;
        }

        @Override
        public void setValue(PreparedStatement st, int startIndex, Null value)
                throws SQLException {

            if(isNotEmpty(getSQLTypes()))
            {
                st.setNull(startIndex, getSQLTypes()[0]);
            }
            else
            {
                throw new RuntimeException("Unable to set database column to null");
            }
        }

        @Override
        public String getLiteral(final Null value)
        {
            return "null";
        }
    }

}
