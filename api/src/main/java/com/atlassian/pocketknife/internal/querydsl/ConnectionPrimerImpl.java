package com.atlassian.pocketknife.internal.querydsl;

import com.atlassian.pocketknife.api.querydsl.ConnectionPrimer;
import com.atlassian.pocketknife.api.querydsl.SchemaProvider;
import com.atlassian.pocketknife.internal.querydsl.schema.DatabaseSchemaCreation;
import com.atlassian.pocketknife.spi.querydsl.DialectConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.sql.Connection;

@Component
public class ConnectionPrimerImpl implements ConnectionPrimer
{
    private final DatabaseSchemaCreation databaseSchemaCreation;
    private final SchemaProvider schemaProvider;
    private final DialectConfiguration dialectConfiguration;

    @Autowired
    public ConnectionPrimerImpl(final DatabaseSchemaCreation databaseSchemaCreation, final SchemaProvider schemaProvider, final DialectConfiguration dialectConfiguration)
    {
        this.databaseSchemaCreation = databaseSchemaCreation;
        this.schemaProvider = schemaProvider;
        this.dialectConfiguration = dialectConfiguration;
    }

    @Override
    public Connection prime(final Connection connection)
    {
        databaseSchemaCreation.prime();
        schemaProvider.prime(connection);
        dialectConfiguration.getDialectConfig(connection);
        return connection;
    }
}
