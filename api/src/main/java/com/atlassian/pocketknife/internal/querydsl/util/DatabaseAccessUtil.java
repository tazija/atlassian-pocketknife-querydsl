package com.atlassian.pocketknife.internal.querydsl.util;

import com.atlassian.pocketknife.api.querydsl.ConnectionProvider;
import com.google.common.base.Function;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

/**
 * We need this side effect to have happened BEFORE we call on PK QueryDSL. This is because of connection deadlocks
 * in production code. The upstream library was changed to ensure that access to "schema information" was done inside a "connection" context
 * otherwise its a state exception.
 *
 * But much of our code was written BEFORE this constraint and it uses Q classes outside a "connection context" and before ANY connections
 * have been used.
 *
 * So this code has been placed here to "prime" the underlying schema information BEFORE access to the Q classes is given out.
 *
 * This could be removed if we changed all the callers to access Q classes inside their "queryFactory" connections contexts.  We could do that
 * if we use EnhancedRelationalPathBase for example and remove all the "withSchema" calls.
 *
 * But for now we have this fix in place.  The use of the atomic boolean just makes it a smidge more efficient.  We only need to get a connection
 * once (-ish).  Its does need to be strictly thread safe but it should only happen once-ish.
 */
@Component
public class DatabaseAccessUtil implements InitializingBean, DisposableBean
{
    private static final AtomicReference<DatabaseAccessUtil> instance = new AtomicReference<>();

    public static void ensurePrimed()
    {
        final DatabaseAccessUtil databaseAccessUtil = instance.get();
        if (databaseAccessUtil == null)
        {
            throw new IllegalStateException("bean is not ready");
        }

        databaseAccessUtil.doEnsurePrimed();
    }

    @Autowired
    private ConnectionProvider connectionProvider;

    private final AtomicBoolean primed = new AtomicBoolean(false);

    @Override
    public void afterPropertiesSet() throws Exception
    {
        instance.set(this);
    }

    @Override
    public void destroy() throws Exception
    {
        instance.set(null);
    }

    /**
     * Ensure the database and the library code that uses it is primed and ready to go
     */
    private void doEnsurePrimed()
    {
        if (!primed.get())
        {
            connectionProvider.withConnection(new Function<Connection, Object>()
            {
                @Override
                public Object apply(final Connection connection)
                {
                    return primed.getAndSet(true);
                }
            });
        }
    }

}
