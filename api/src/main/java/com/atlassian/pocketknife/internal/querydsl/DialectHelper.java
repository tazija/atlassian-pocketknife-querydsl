package com.atlassian.pocketknife.internal.querydsl;

import com.atlassian.fugue.Pair;
import com.atlassian.pocketknife.api.querydsl.DialectProvider;
import com.mysema.query.sql.SQLServer2005Templates;
import com.mysema.query.sql.SQLServer2008Templates;
import com.mysema.query.sql.SQLServer2012Templates;
import com.mysema.query.sql.SQLServerTemplates;
import com.mysema.query.sql.SQLTemplates;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.sql.DatabaseMetaData;
import java.sql.SQLException;

/**
 * Dialect helper provides facilities for instantiating dialect Configuration
 */
public class DialectHelper
{
    private static final Logger log = LoggerFactory.getLogger(DialectHelper.class);

    public static final String DB_URL_IDENTIFIER_POSTGRES = ":postgresql:";
    public static final String DB_URL_IDENTIFIER_ORACLE = ":oracle:";
    public static final String DB_URL_IDENTIFIER_HSQLDB = ":hsqldb:";
    public static final String DB_URL_IDENTIFIER_SQLSERVER = ":sqlserver:";
    public static final String DB_URL_IDENTIFIER_MYSQL = ":mysql:";
    public static final String DB_URL_IDENTIFIER_H2 = ":h2:";
    public static final String DB_URL_IDENTIFIER_NUODB = ":com.nuodb:";

    // sqlserver product version number (ref https://support.microsoft.com/en-us/kb/321185)
    public static final int SQLSERVER_2005 = 9;
    public static final int SQLSERVER_2008 = 10;
    public static final int SQLSERVER_2012 = 11;

    public static boolean isSQLServer(@Nonnull final String connStr)
    {
        return connStr.contains(DB_URL_IDENTIFIER_SQLSERVER);
    }

    /**
     * Get DB template for SQLServer
     *
     * @param metaData Database connection data
     * @return A template builder that is applicable to the current SQLServer
     * @throws SQLException if database access error occurs
     */
    public static Pair<SQLTemplates.Builder, DialectProvider.SupportedDatabase> getSQLServerDBTemplate(@Nonnull final DatabaseMetaData metaData) throws SQLException
    {
        int currentSqlServerVersion = metaData.getDatabaseMajorVersion();
        log.debug("Initialize SQLServer template for version {}", currentSqlServerVersion);

        if (currentSqlServerVersion >= SQLSERVER_2012)
        {
            return Pair.pair(SQLServer2012Templates.builder().printSchema(), DialectProvider.SupportedDatabase.SQLSERVER);
        }
        else if (currentSqlServerVersion >= SQLSERVER_2008)
        {
            return Pair.pair(SQLServer2008Templates.builder().printSchema(), DialectProvider.SupportedDatabase.SQLSERVER);
        }
        else if (currentSqlServerVersion >= SQLSERVER_2005)
        {
            return Pair.pair(SQLServer2005Templates.builder().printSchema(), DialectProvider.SupportedDatabase.SQLSERVER);
        }
        return Pair.pair(SQLServerTemplates.builder().printSchema(), DialectProvider.SupportedDatabase.SQLSERVER);
    }
}
