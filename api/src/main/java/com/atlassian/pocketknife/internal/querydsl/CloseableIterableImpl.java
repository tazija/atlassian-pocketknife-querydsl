package com.atlassian.pocketknife.internal.querydsl;

import com.atlassian.annotations.Internal;
import com.atlassian.fugue.Effect;
import com.atlassian.fugue.Option;
import com.atlassian.pocketknife.api.querydsl.ClosePromise;
import com.atlassian.pocketknife.api.querydsl.CloseableIterable;
import com.google.common.base.Function;
import com.google.common.base.Functions;
import com.google.common.base.Preconditions;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.mysema.commons.lang.CloseableIterator;

import java.util.NoSuchElementException;

/**
 * Tuple functions are implemented in terms of this class.  Its a generic implementation of a {@link
 * com.atlassian.pocketknife.api.querydsl.CloseableIterable}
 */
@Internal
public class CloseableIterableImpl<S,T> implements CloseableIterable<T>
{
    private final CloseableIteratorImpl<S, T> closeableIterator;
    private final ClosePromise closePromise;

    public CloseableIterableImpl(CloseableIterator<S> srcIterator, Function<S, T> mapper, final ClosePromise parentPromise)
    {
        this(srcIterator, mapper, parentPromise, Predicates.<S>alwaysTrue(), Predicates.<S>alwaysTrue());
    }

    public CloseableIterableImpl(CloseableIterator<S> srcIterator, Function<S, T> mapper, final ClosePromise parentPromise, Predicate<S> filterPredicate, Predicate<S> takeWhilePredicate)
    {
        IteratorInstructions<S, T> instructions = new IteratorInstructions<>(srcIterator, mapper, parentPromise, filterPredicate, takeWhilePredicate);

        this.closeableIterator = new CloseableIteratorImpl<>(instructions);
        this.closePromise = new ClosePromise(parentPromise, new Runnable()
        {
            @Override
            public void run()
            {
                closeImpl();
            }
        });
    }

    public CloseableIterableImpl(CloseableIteratorImpl<S, T> closeableIterator)
    {
        this.closeableIterator = closeableIterator;
        this.closePromise = closeableIterator.closePromise;
    }

    @Override
    public CloseableIterator<T> iterator()
    {
        return closeableIterator;
    }

    @Override
    public ClosePromise getClosePromise()
    {
        return closePromise;
    }

    @Override
    public CloseableIterable<T> take(final int n)
    {
        Preconditions.checkArgument(n >= 0, "take(n) argument must be >= 0");
        Preconditions.checkState(!this.closeableIterator.hasStarted(), "You cant take(n) from an iterable that has been read");

        Predicate<T> nTaken = nTakenPredicate(n);
        return takeWhile(nTaken);
    }

    @Override
    public CloseableIterable<T> takeWhile(final Predicate<T> takeWhilePredicate)
    {
        Preconditions.checkNotNull(takeWhilePredicate);
        Preconditions.checkState(!this.closeableIterator.hasStarted(), "You cant takeWhile() from an iterable that has been read");

        final CloseableIteratorImpl<S, T> src = this.closeableIterator;

        Predicate<S> composedTakeWhile = Predicates.compose(takeWhilePredicate, new Function<S, T>()
        {
            @Override
            public T apply(final S input)
            {
                return src.mapper.apply(input);
            }
        });
        IteratorInstructions<S, T> instructions = new IteratorInstructions<>(src.instructions);
        instructions.takeWhilePredicate = composedTakeWhile;

        CloseableIteratorImpl<S, T> newIterator = new CloseableIteratorImpl<>(instructions);
        return new CloseableIterableImpl<>(newIterator);
    }

    @Override
    public CloseableIterable<T> filter(Predicate<T> filterPredicate)
    {
        Preconditions.checkNotNull(filterPredicate);

        final CloseableIteratorImpl<S, T> src = this.closeableIterator;

        Predicate<S> composedFilterPredicate = Predicates.compose(filterPredicate, new Function<S, T>()
        {
            @Override
            public T apply(final S input)
            {
                return src.mapper.apply(input);
            }
        });
        IteratorInstructions<S, T> instructions = new IteratorInstructions<>(src.instructions);
        instructions.filterPredicate = composedFilterPredicate;

        CloseableIteratorImpl<S, T> newIterator = new CloseableIteratorImpl<>(instructions);
        return new CloseableIterableImpl<>(newIterator);
    }

    /**
     * A predicate that counts how many things go past and returns false once enough have
     * @param n the max number inclusive
     * @param <T> a type
     * @return true if less than n have been taken
     */
    public static <T> Predicate<T> nTakenPredicate(final int n)
    {
        return new Predicate<T>()
        {
            int takenSoFar = 0;

            @Override
            public boolean apply(final T s)
            {
                if (takenSoFar < n)
                {
                    takenSoFar++;
                    return true;
                }
                return false;
            }
        };
    }

    @Override
    public <D> CloseableIterable<D> map(final Function<T, D> mapper)
    {
        // we need to map from the old iterable of S,T to a new one of T,D
        final CloseableIteratorImpl<S, T> src = this.closeableIterator;

        final Function<S, D> composedMapper = new Function<S, D>()
        {
            @Override
            public D apply(final S input)
            {
                return Functions.compose(mapper, src.mapper).apply(input);
            }
        };
        IteratorInstructions<S, D> instructions = new IteratorInstructions<>(src.srcIterator, composedMapper, closePromise, src.filterPredicate, src.takeWhilePredicate);

        CloseableIteratorImpl<S, D> composedIterator = new CloseableIteratorImpl<>(instructions);
        return new CloseableIterableImpl<>(composedIterator);
    }

    @Override
    public Option<T> fetchFirst()
    {
        if (!closePromise.isClosed())
        {
            try
            {
                CloseableIterator<T> iterator = iterator();
                if (iterator.hasNext())
                {
                    return Option.some(iterator.next());
                }
            }
            finally
            {
                closePromise.close();
            }
        }
        return Option.none();
    }

    @Override
    public void foreach(final Effect<T> effect)
    {
        if (!closePromise.isClosed())
        {
            try
            {
                for (final T t : this)
                {
                    effect.apply(t);
                }
            }
            finally
            {
                closePromise.close();
            }
        }
    }

    @Override
    public void close()
    {
        closePromise.close();
    }

    private void closeImpl()
    {
        closeableIterator.close();
    }

    /**
     * A simpler holder class that has the innards of the Iterator
     */
    static class IteratorInstructions<S, T>
    {
        private CloseableIterator<S> srcIterator;
        private Function<S, T> mapper;
        private ClosePromise closePromise;
        private Predicate<S> filterPredicate;
        private Predicate<S> takeWhilePredicate;

        public IteratorInstructions(final CloseableIterator<S> srcIterator, final Function<S, T> mapper, final ClosePromise closePromise, final Predicate<S> filterPredicate, final Predicate<S> takeWhilePredicate)
        {
            this.srcIterator = srcIterator;
            this.mapper = mapper;
            this.closePromise = closePromise;
            this.filterPredicate = filterPredicate;
            this.takeWhilePredicate = takeWhilePredicate;
        }

        IteratorInstructions(IteratorInstructions<S, T> instructions)
        {
            this.srcIterator = instructions.srcIterator;
            this.mapper = instructions.mapper;
            this.closePromise = instructions.closePromise;
            this.takeWhilePredicate = instructions.takeWhilePredicate;
            this.filterPredicate = instructions.filterPredicate;
        }
    }

    /**
     * The underlying Iterator that maps from S to T
     *
     * @param <S> the S-ness of S eg the source
     * @param <T> the T-ness of T eg the target
     */
    static class CloseableIteratorImpl<S, T> implements CloseableIterator<T>
    {
        private final CloseableIterator<S> srcIterator;
        private final Function<S, T> mapper;
        private final ClosePromise closePromise;
        private final Predicate<S> filterPredicate;
        private final Predicate<S> takeWhilePredicate;
        private final IteratorInstructions<S, T> instructions;

        int returnedSoFar;
        S nextObject;
        boolean nextObjectAccessed = true;

        CloseableIteratorImpl(IteratorInstructions<S, T> instructions)
        {
            this.instructions = instructions;
            this.srcIterator = instructions.srcIterator;
            this.mapper = instructions.mapper;
            this.closePromise = instructions.closePromise;
            this.filterPredicate = instructions.filterPredicate;
            this.takeWhilePredicate = instructions.takeWhilePredicate;
            this.returnedSoFar = 0;
        }


        boolean hasStarted()
        {
            return returnedSoFar > 0;
        }

        @Override
        public boolean hasNext()
        {
            if (!nextObjectAccessed)
            {
                return true;
            }

            if (closePromise.isClosed())
            {
                return false;
            }
            boolean hasNext = srcIterator.hasNext();
            //
            // continue to read as we filter 'in' the next object
            while (hasNext)
            {
                this.nextObject = nextImpl();
                this.nextObjectAccessed = false;

                if (filterPredicate.apply(this.nextObject))
                {
                    break;
                }
                hasNext = srcIterator.hasNext();
            }
            //
            // and then if its acceptable decide whether we have taken enough
            if (hasNext && !takeWhilePredicate.apply(this.nextObject))
            {
                hasNext = false;
            }
            //
            // finally if we have no next then close
            if (!hasNext)
            {
                this.nextObject = null;
                close();
            }
            return hasNext;
        }

        private S nextImpl()
        {
            S next = srcIterator.next();
            returnedSoFar++;
            return next;
        }

        @Override
        public T next()
        {
            if (!hasNext())
            {
                throw new NoSuchElementException();
            }
            nextObjectAccessed = true;
            return mapper.apply(this.nextObject);
        }

        @Override
        public void close()
        {
            closePromise.close();
        }

        @Override
        public void remove()
        {
            throw new UnsupportedOperationException();
        }
    }
}
