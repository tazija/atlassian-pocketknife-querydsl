package com.atlassian.pocketknife.internal.querydsl.schema;

import com.atlassian.fugue.Effect;
import com.atlassian.fugue.Option;
import com.atlassian.pocketknife.internal.querydsl.util.MemoizingResettingReference;
import com.atlassian.pocketknife.internal.querydsl.util.Unit;
import com.google.common.base.Function;
import org.joor.Reflect;
import org.joor.ReflectException;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import static com.atlassian.fugue.Option.option;
import static com.atlassian.pocketknife.internal.querydsl.util.Unit.VALUE;

/**
 * As much as QueryDSL is an attempt to move away from inActiveObjects, this class is needed for a very good reason. All
 * QueryDSL code needs database tables to run against and inActiveObjects is still the mechanism in which we take a
 * schema and apply to onto the database.
 * <p/>
 * This class is needed because of the race condition this brings.  We need the database schema to be applied before we
 * read / write from those tables.  So we must "join" to iAO to that we can be sure it has happened.
 * <p/>
 * This class is responsible for that.  Its lazy and caches its call so it happens only once.
 * <p/>
 * See : https://jdog.jira-dev.com/browse/JPA-411 for more low level details of the problems
 * <p/>
 * In the future we may have another mechanism for creating "database schemas" but the problem still remains. We MUST
 * ensure it has run before we access tables.
 */
@Component
public class DatabaseSchemaCreationImpl implements DatabaseSchemaCreation
{
    private static final Logger log = LoggerFactory.getLogger(DatabaseSchemaCreationImpl.class);

    private static final String AO_SERVICE_NAME = "com.atlassian.activeobjects.external.ActiveObjects";

    private final MemoizingResettingReference<Unit, Unit> initialised;
    private final BundleContext bundleContext;

    @Autowired
    public DatabaseSchemaCreationImpl(final BundleContext bundleContext)
    {
        this.bundleContext = bundleContext;
        this.initialised = new MemoizingResettingReference<>(primeImpl());
    }

    public void prime()
    {
        initialised.get(VALUE);
    }

    private Function<Unit, Unit> primeImpl()
    {
        return new Function<Unit, Unit>()
        {
            @Override
            public Unit apply(final Unit input)
            {
                // invoke AO if its available in the OSGi system
                getService(AO_SERVICE_NAME).foreach(new Effect<Object>()
                {
                    @Override
                    public void apply(final Object ao)
                    {
                        invokeAo(ao);
                    }
                });
                return VALUE;
            }
        };
    }

    private Option<Object> getService(String serviceName)
    {
        Option<ServiceReference> sRef = option(bundleContext.getServiceReference(serviceName));
        if (sRef.isDefined())
        {
            return option(bundleContext.getService(sRef.get()));
        }
        return Option.none();
    }

    private void invokeAo(final Object ao)
    {
        try
        {
            log.debug("ActiveObjects found - invoking via reflection....");

            // this calls flushAll because PKQDSL is based on older products
            // newer products could call ActiveObjectsModuleMetaData to do this but
            // for the sake of one line - we make this call and it has the same effect
            Reflect.on(ao).call("flushAll");
        }
        catch (ReflectException e)
        {
            log.warn("ActiveObjects method flushAll is not available : " + e.toString());
        }
    }
}
