package com.atlassian.pocketknife.internal.querydsl;

import com.atlassian.pocketknife.api.querydsl.SchemaProvider;
import com.atlassian.pocketknife.internal.querydsl.util.MemoizingResettingReference;
import com.google.common.base.Function;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.Nonnull;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static org.apache.commons.lang.StringUtils.isEmpty;

@Component
public class DefaultSchemaProvider implements SchemaProvider
{
    private static final String SCHEMA_NAME_KEY = "TABLE_SCHEM";
    private static final String TABLE_NAME_KEY = "TABLE_NAME";
    private static final String COLUMN_NAME_KEY = "COLUMN_NAME";

    private final MemoizingResettingReference<Connection, Map<String, String>> tableToSchemaRef;
    private final MemoizingResettingReference<Connection, Map<NameKey, String>> tableColumnNamesRef;

    public DefaultSchemaProvider()
    {
        this.tableToSchemaRef = new MemoizingResettingReference<>(tableToSchemaFunction());
        this.tableColumnNamesRef = new MemoizingResettingReference<>(tableColumnsNamesFunction());
    }

    @Override
    public void prime(final Connection connection)
    {
        tableToSchemaRef.get(connection);
        tableColumnNamesRef.get(connection);
    }

    @Override
    public String getSchema(@Nonnull final String logicalTableName)
    {
        checkArgument(!isEmpty(logicalTableName), "Table name is required");

        Map<String, String> tableToSchema = tableSchemaRef();

        String tableName = tableToSchema.get(logicalTableName.toUpperCase());
        if (tableName != null)
        {
            return tableName;
        }
        throw new IllegalArgumentException(String.format("Not able to find table %s", logicalTableName));
    }

    @Override
    public String getTableName(@Nonnull final String logicalTableName)
    {
        checkArgument(!isEmpty(logicalTableName), "Table name is required");
        Map<NameKey, String> tableColumnNames = tableColumnNamesRef();

        NameKey key = new NameKey(logicalTableName);
        return tableColumnNames.get(key);
    }

    @Override
    public String getColumnName(@Nonnull final String logicalTableName, @Nonnull final String logicalColumnName)
    {
        checkArgument(!isEmpty(logicalTableName), "Table name is required");
        checkArgument(!isEmpty(logicalColumnName), "Column name is required");
        Map<NameKey, String> tableColumnNames = tableColumnNamesRef();

        NameKey key = new NameKey(logicalTableName, logicalColumnName);
        return tableColumnNames.get(key);
    }

    private Map<String, String> tableSchemaRef()
    {
        try
        {
            return tableToSchemaRef.getMemoizedValue();
        }
        catch (MemoizingResettingReference.MemoizedValueNotPresentException e)
        {
            throw new UnprimedSchemaProviderException(e);
        }
    }

    private Map<NameKey, String> tableColumnNamesRef()
    {
        try
        {
            return tableColumnNamesRef.getMemoizedValue();
        }
        catch (MemoizingResettingReference.MemoizedValueNotPresentException e)
        {
            throw new UnprimedSchemaProviderException(e);
        }
    }

    class UnprimedSchemaProviderException extends RuntimeException
    {
        public UnprimedSchemaProviderException(final Throwable cause)
        {
            super(""
                    + "A call to SchemaProvider.prime(connection) MUST be made before schema information can be consumed.\n"
                    + "This can occur if you use SchemaProviderAccessor / EnhancedRelationalPathBase in a static context before the database is available.\n"
                    + "Remember the database may not be available during code class initialisation. \n"
                    + "Avoid eager static initialisation when using SchemaProviderAccessor / EnhancedRelationalPathBase to avoid this problem \n"
                    , cause);
        }
    }

    private Function<Connection, Map<String, String>> tableToSchemaFunction()
    {
        return new Function<Connection, Map<String, String>>()
        {
            @Override
            public Map<String, String> apply(final Connection connection)
            {
                return tablesToSchema(connection);
            }
        };
    }

    private Map<String, String> tablesToSchema(final Connection connection)
    {
        try
        {
            final Map<String, String> result = new HashMap<>();
            try (ResultSet resultSet = connection.getMetaData().getTables(null, null, null, null))
            {
                while (resultSet.next())
                {
                    final String tableName = resultSet.getString(TABLE_NAME_KEY);
                    final String tableSchema = resultSet.getString(SCHEMA_NAME_KEY);
                    String schemaName = "";
                    if (StringUtils.isNotEmpty(tableSchema))
                    {
                        schemaName = tableSchema;
                    }
                    result.put(tableName.toUpperCase(), schemaName);
                }
            }
            return result;
        }
        catch (SQLException sqlEx)
        {
            throw new RuntimeException("Unable to enquire table names available in the system",sqlEx);
        }
    }

    private Function<Connection, Map<NameKey, String>> tableColumnsNamesFunction()
    {
        return new Function<Connection, Map<NameKey, String>>()
        {
            @Override
            public Map<NameKey, String> apply(final Connection connection)
            {
                return tableColumnNames(connection);
            }
        };
    }

    private Map<NameKey, String> tableColumnNames(final Connection connection)
    {
        try
        {
            final Map<NameKey, String> result = new HashMap<>();
            final DatabaseMetaData metaData = connection.getMetaData();

            findTableNames(metaData, result);
            findColumnNames(metaData, result);

            return result;
        }
        catch (SQLException sqlEx)
        {
            throw new RuntimeException("Unable to enquire table names available in the system",sqlEx);
        }
    }

    private void findColumnNames(final DatabaseMetaData metaData, final Map<NameKey, String> result) throws SQLException
    {
        try (ResultSet columnResultSet = metaData.getColumns(null, null, null, null))
        {
            while (columnResultSet.next())
            {
                final String tableName = columnResultSet.getString(TABLE_NAME_KEY);
                final String columnName = columnResultSet.getString(COLUMN_NAME_KEY);
                result.put(new NameKey(tableName, columnName), columnName);
            }
        }
    }

    private void findTableNames(final DatabaseMetaData metaData, final Map<NameKey, String> result) throws SQLException
    {
        try (ResultSet resultSet = metaData.getTables(null, null, null, null))
        {
            while (resultSet.next())
            {
                final String tableName = resultSet.getString(TABLE_NAME_KEY);
                result.put(new NameKey(tableName), tableName);
            }
        }
    }

    private static class NameKey
    {
        private final String tableName;
        private final String columnName;


        private NameKey(@Nonnull final String tableName)
        {
            this(tableName, null);
        }

        private NameKey(@Nonnull final String tableName, final String columnName)
        {

            this.tableName = checkNotNull(tableName).toUpperCase();
            this.columnName = columnName == null ? null : columnName.toUpperCase();
        }

        @Override
        public boolean equals(final Object obj)
        {
            if (this == obj) { return true; }
            if (obj == null || obj.getClass() != getClass()) { return false; }

            NameKey other = (NameKey) obj;

            return tableName.equals(other.tableName) && (columnName == null ? (other.columnName == null) : columnName.equals(other.columnName));
        }

        @Override
        public int hashCode()
        {
            int result = tableName.hashCode();
            result = 31 * result + (columnName != null ? columnName.hashCode() : 0);
            return result;
        }
    }

}
