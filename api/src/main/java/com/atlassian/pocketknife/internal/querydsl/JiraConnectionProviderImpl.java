package com.atlassian.pocketknife.internal.querydsl;

import com.atlassian.jira.ofbiz.DefaultOfBizConnectionFactory;
import com.atlassian.plugin.spring.scanner.annotation.component.JiraComponent;
import com.atlassian.pocketknife.api.querydsl.ConnectionPrimer;
import com.atlassian.pocketknife.spi.querydsl.AbstractConnectionProvider;
import com.google.common.annotations.VisibleForTesting;
import org.springframework.beans.factory.annotation.Autowired;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * JIRA rules the waves!
 */
@JiraComponent
public class JiraConnectionProviderImpl extends AbstractConnectionProvider
{
    @Autowired
    public JiraConnectionProviderImpl(ConnectionPrimer connectionPrimer)
    {
        super(connectionPrimer);
    }

    protected Connection getConnectionImpl(final boolean autoCommit)
    {
        DefaultOfBizConnectionFactory instance = getDefaultOfBizConnectionFactory();

        log().debug("Getting a connection with auto-commit '" + autoCommit +
                "' using a DefaultOfBizConnectionFactory");

        try
        {
            Connection connection = instance.getConnection();
            connection.setAutoCommit(autoCommit);

            log().debug("Got connection with auto-commit '" + autoCommit + "'");

            return connection;
        }
        catch (SQLException e)
        {
            throw new RuntimeException(e);
        }
    }

    @VisibleForTesting
    DefaultOfBizConnectionFactory getDefaultOfBizConnectionFactory()
    {
        return DefaultOfBizConnectionFactory.getInstance();
    }
}
