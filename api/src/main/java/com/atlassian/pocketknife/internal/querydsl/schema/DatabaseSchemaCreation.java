package com.atlassian.pocketknife.internal.querydsl.schema;

/**
 * Ensures the underlying database schema is created before tables are accessed
 */
public interface DatabaseSchemaCreation
{
    /**
     * A initialisation call to ensure the database schema creation has run
     */
    public void prime();
}
