package com.atlassian.pocketknife.internal.querydsl;

import com.atlassian.fugue.Effect;
import com.atlassian.fugue.Function2;
import com.atlassian.fugue.Option;
import com.atlassian.pocketknife.api.querydsl.ClosePromise;
import com.atlassian.pocketknife.api.querydsl.CloseableIterable;
import com.atlassian.pocketknife.api.querydsl.StreamyResult;
import com.atlassian.pocketknife.api.querydsl.Tuples;
import com.google.common.base.Function;
import com.google.common.base.Functions;
import com.google.common.base.Predicate;
import com.mysema.commons.lang.CloseableIterator;
import com.mysema.query.Tuple;
import com.mysema.query.types.Expression;

/**
 */
public class StreamyResultImpl implements StreamyResult
{
    private final CloseableIterator<Tuple> closeableIterator;
    private final ClosePromise closePromise;

    public StreamyResultImpl(final CloseableIterator<Tuple> closeableIterator, final ClosePromise parentPromise)
    {
        this.closeableIterator = closeableIterator;
        this.closePromise = new ClosePromise(parentPromise, new Runnable()
        {
            @Override
            public void run()
            {
                closeableIterator.close();
            }
        });
    }

    private Function<Tuple, Tuple> identity()
    {
        return Functions.identity();
    }

    @Override
    public CloseableIterator<Tuple> iterator()
    {
        CloseableIterable<Tuple> iterable = map(identity());
        return iterable.iterator();
    }

    @Override
    public ClosePromise getClosePromise()
    {
        return closePromise;
    }

    @Override
    public <D> CloseableIterable<D> map(final Function<Tuple, D> mapper)
    {
        if (closePromise.isClosed())
        {
            throw new IllegalStateException("This streaming result has already been closed");
        }
        return Tuples.map(closeableIterator, mapper, closePromise);
    }

    @Override
    public <D> CloseableIterable<D> map(final Expression<D> expr)
    {
        return map(Tuples.column(expr));
    }

    @Override
    public CloseableIterable<Tuple> take(final int n)
    {
        if (closePromise.isClosed())
        {
            throw new IllegalStateException("This streaming result has already been closed");
        }
        return Tuples.take(closeableIterator, identity(),closePromise,n);
    }

    @Override
    public CloseableIterable<Tuple> takeWhile(final Predicate<Tuple> takeWhilePredicate)
    {
        if (closePromise.isClosed())
        {
            throw new IllegalStateException("This streaming result has already been closed");
        }
        return Tuples.takeWhile(closeableIterator, identity(), closePromise, takeWhilePredicate);
    }

    @Override
    public CloseableIterable<Tuple> filter(final Predicate<Tuple> filterPredicate)
    {
        if (closePromise.isClosed())
        {
            throw new IllegalStateException("This streaming result has already been closed");
        }
        return Tuples.filter(closeableIterator, identity(), closePromise, filterPredicate);
    }

    @Override
    public Option<Tuple> fetchFirst()
    {
        if (closePromise.isClosed())
        {
            throw new IllegalStateException("This streaming result has already been closed");
        }
        try
        {
            CloseableIterator<Tuple> iterator = iterator();
            if (iterator.hasNext())
            {
                return Option.some(iterator.next());
            }
        }
        finally
        {
            closePromise.close();
        }
        return Option.none();
    }

    @Override
    public void foreach(final Effect<Tuple> effect)
    {
        if (closePromise.isClosed())
        {
            throw new IllegalStateException("This streaming result has already been closed");
        }
        try
        {
            for (final Tuple tuple : this)
            {
                effect.apply(tuple);
            }
        }
        finally
        {
            closePromise.close();
        }
    }

    @Override
    public <T> T foldLeft(final T initial, Function2<T, Tuple, T> combiningFunction)
    {
        if (closePromise.isClosed())
        {
            throw new IllegalStateException("This streaming result has already been closed");
        }

        try
        {
            T accumulator = initial;
            while (closeableIterator.hasNext())
            {
                accumulator = combiningFunction.apply(accumulator, closeableIterator.next());
            }
            return accumulator;
        }
        finally
        {
            closePromise.close();
        }
    }

    @Override
    public void close()
    {
        closePromise.close();
    }
}
