# Overview #
This is the querydsl part of the pocketknife suite of libraries, see [https://bitbucket.org/atlassian/atlassian-pocketknife](https://bitbucket.org/atlassian/atlassian-pocketknife) for details of the original project

This component provides utilities and scaffolding for working with [QueryDSL](http://www.querydsl.com/) within the Atlassian ecosystem.

# Contribution #

All pull requests are expected to have a green branch build in the [pocketknife-querydsl CI build](https://servicedesk-bamboo.internal.atlassian.com/browse/PKQDSL). Automatic releasing via bamboo is not configured at this stage.

# Getting started #
The main components are available in the `com.atlassian.pocketknife.api.querydsl` package.

The main entry point to the library is:

* `QueryFactory`

# SQL Querying Code Examples #

To get an understanding of QueryDSL syntax in general [look at this QueryDSL documentation ] (http://www.querydsl.com/static/querydsl/latest/reference/html/ch02s03.html)

`QueryFactory` is the mechanism to execute SQL statements.  By default it will use the underlying wired `ConnectionProvider` to obtain connections
on your behalf.

    List<Tuple> result = queryFactory.select(select -> select.from(EMPLOYEE).list(EMPLOYEE.FIRST_NAME, EMPLOYEE.LAST_NAME));

This is often the best way because the lifecycle of the connection is managed for you.

But if you want to manage the connection yourself then you can pass it explicitly

    List<Tuple> result = queryFactory.select(connection).from(EMPLOYEE).list(EMPLOYEE.FIRST_NAME, EMPLOYEE.LAST_NAME);

You will then be responsible for the lifecycle of this connection.

The above examples load all the results into memory.  You can stream the results instead via `StreamyResult`, loading only one row into memory
at a time.

    StreamyResult result = queryFactory.select(select -> select.from(EMPLOYEE)
                                                   .where(EMPLOYEE.LAST_NAME.contains("Smith"))
                                                   .stream(EMPLOYEE.ID, EMPLOYEE.FIRST_NAME, EMPLOYEE.LAST_NAME);

A `StreamyResult` is a `CloseableIterable` and hence can be iterated on

     for (Tuple tuple : result ) {
        // consume each Tuple
        System.out.println(tuple.get(EMPLOYEE.FIRST_NAME));
     }

Once the stream is consumed, the underlying result set will be closed and the connection returned to the pool.  Like any limited resource
operation it is super important that you do this safely, especially if you code throws Exceptions or short cuts out of the result consumption.

Java 7 try () with resources statements make this easy since `StreamyResult` is `AutoCloseable`.

    try (StreamyResult result = queryFactory.select(select -> select.from(EMPLOYEE)
                                                            .where(EMPLOYEE.LAST_NAME.contains("Smith"))
                                                            .stream(EMPLOYEE.ID, EMPLOYEE.FIRST_NAME, EMPLOYEE.LAST_NAME))
    {
         for (Tuple t : result ) {
            // consume each Tuple
         }
    
    }

A QueryDSL `Tuple` is a raw result object analogous to a JDBC `ResultSet`.  You can map this into specific objects via the `map()` method

    final List<Employee> employees = new ArrayList<>();
    try (StreamyResult streamyResult = createStreamyQuery())
    {
        for (EMPLOYEE e : streamyResult.map(tuple ->
            {
                Employee e = new EMPLOYEE(tuple.get(EMPLOYEE.ID));
                e.setFirstname(tuple.get(EMPLOYEE.FIRST_NAME));
                e.setLastname(tuple.get(EMPLOYEE.LAST_NAME));
                return e;
            }
        })
        {
            employees.add(e);
        }
    }
    
A variation on mapping is when you want only one type of column based on a single QueryDSL table column
    
        CloseableIterator<String> firstNames = streamyResult.map(EMPLOYEE.FIRST_NAME);

    
You can obtain just the first row via the `fetchFirst` method.  Since this is a terminal operation, this will close the stream of results
once called.
    
    Option<String> firstName = streamyResult.map(EMPLOYEE.FIRST_NAME).fetchFirst();

You can take only a limited set of rows by using the `take` method.  Its comes in two flavours.  The first is a simple number of rows

    CloseableIterator<String> firstNames = streamyResult.map(EMPLOYEE.FIRST_NAME).take(50);

The second method is `takeWhile` which has a predicate as an argument and returns rows until the predicate is no longer true


    Predicate<String> containsM = new Predicate<String>()
    {
        @Override
        public boolean apply(final String input)
        {
            return input.contains("M");
        }
    };
    CloseableIterator<String> firstNames = streamyResult.map(EMPLOYEE.FIRST_NAME).takeWhile(containsM);

Both the `take` and `takeWhile` methods terminate the underlying closeable iterable.

There is a `filter` method that will only returns elements that match a predicate/

    CloseableIterator<String> firstNames = streamyResult.map(EMPLOYEE.FIRST_NAME).filter(name -> name.contains("M");

One thing to remember is that limiting your result sets and filtering them is a job BEST done by the SQL database.  The `take` and `filter` methods should only be used
when you are unable to filter and limit in SQL terms.

You can use side effects to process the results via the `foreach` method

        final List<String> firstNames = new ArrayList<>();
        streamyResult.map(EMPLOYEE.FIRST_NAME).foreach(s -> firstNames.add(s));


There are functions to perform a fold on a stream of results

    Integer count = streamyResult.foldLeft(0, (accumulator, tuple) -> return accumulator + 1);

# SQL DML Examples #

SQL inserts, updates and deletes can also be performed by the `QueryFactory`.  You need to give it the entity to act on and
a function that creates and performs the SQL

        Long qId = queryFactory.insert(QUEUE, insert ->
                insert
                .set(QUEUE.NAME, queueDef.getName())
                .set(QUEUE.PURPOSE, queueDef.getPurpose())
                .set(QUEUE.CREATED_TIME, System.currentTimeMillis())
                .executeWithKey(QUEUE.ID);


        queryFactory.delete(MESSAGE, delete -> delete.where(MESSAGE.QUEUE_ID.eq(queueId)).execute());


# Entity Code Examples #

The tables in the system are represented by QueryDSL RelationalPath definitions.  This library has added the 'EnhancedRelationalPathBase'
extension class to make declaring these even easier.

    public class MessageTable extends EnhancedRelationalPathBase<MessageTable>
    {
        // Columns
        public final NumberPath<Long> ID = createLongCol("ID").asPrimaryKey().build();
        public final NumberPath<Long> QUEUE_ID = createLongCol("QUEUE_ID").notNull().build();
        public final NumberPath<Long> CREATED_TIME = createLongCol("CREATED_TIME").notNull().build();
        public final NumberPath<Long> EXPIRY_TIME = createLongCol("EXPIRY_TIME").build();
        public final StringPath CONTENT_TYPE = createStringCol("CONTENT_TYPE").notNull().build();
        public final NumberPath<Integer> VERSION = createIntegerCol("VERSION").notNull().build();
        public final NumberPath<Integer> PRIORITY = createIntegerCol("PRIORITY").notNull().build();
        public final StringPath MSG_ID = createStringCol("MSG_ID").notNull().build();
        public final NumberPath<Long> MSG_LENGTH = createLongCol("MSG_LENGTH").notNull().build();
        public final StringPath MSG_DATA = createStringCol("MSG_DATA").build();

        public MessageTable(final String tableName)
        {
            super(MessageTable.class, tableName);
        }
    }

The above defines a primary key and the set of non null columns in the table.

A common pattern is to then have a Tables class that declares instances of these RelationPath objects

    /**
     * Our QueryDSL tables
     */
    public class Tables
    {
        private static final String AO = "AO_319474_";  // <-- our AO namespace

        public static final QueueTable QUEUE = new QueueTable(AO + "QUEUE");
        public static final MessageTable MESSAGE = new MessageTable(AO + "MESSAGE");
        public static final MessagePropertyTable MESSAGE_PROPERTY = new MessagePropertyTable(AO + "MESSAGE_PROPERTY");
    }

With static imports you are then able to make your database code more SQL-like

    import static com.code.querydsl.Tables.QUEUE;


    return queryFactory.select(txnContext.connection(), select -> select.from(QUEUE)
            .where(QUEUE.NAME.eq(queueName))
            .stream(QUEUE.all()))
            .map(tuple -> new QueueDef(tuple.get(QUEUE.ID), tuple.get(QUEUE.NAME), tuple.get(QUEUE.PURPOSE)));



# Decision Notes #

You may be asking why `StreamyResult` is not a Java 1.8 `Stream`?  This library pre-dates JDK 8 being available and hence
the StreamyResult was invented.  Its inspired by JDK 1.8 `Stream` no doubt.  However much of the `Predicate` filtering and grouping
that can be done on `Stream`s are in fact better down at the SQL layer rather than in Java.  So these methods have not been added
to StreamyResult.

The same pre-dated JDK 1.8 rationale applies to the library's use of Guava functions and Fugue Options.  Perhaps in a future
major version we can use Java 8 constructs instead.




